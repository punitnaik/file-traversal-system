package arangodb

const GetDocByExt = `
	FOR doc IN @@collection
	FILTER doc.extension == @extension
	RETURN doc
`

const Query = `
	FOR doc IN @@collection
	COLLECT fileType = doc.extension WITH COUNT INTO count
	RETURN { fileType, count }
`
const GetDocByFilepath = `
	FOR doc IN @@collection
	FILTER doc.Path == @fileName
	RETURN doc
`

const GetLastDocument = `
	FOR doc IN @@collection 
	SORT doc._modified DESC 
	LIMIT 1 
	RETURN doc
`
const GetDocFromPath = `
	FOR doc IN @@collection 
	FILTER doc.Path == @filename 
	LIMIT 1
	RETURN doc
`
const GetContainsDocFrom_to = `
	FOR doc IN @@collection 
	FILTER doc._to == @ID 
	LIMIT 1
	RETURN doc
`

const GetRepoNameFromRepoID = `
	FOR doc IN @@collection
	FILTER doc.RepoID == @repoID
	LIMIT 1
	RETURN doc.RepoName
`
const GetLastCommitFromRepoID = `
	FOR doc IN @@collection
	FILTER doc.RepoID == @repoID
	SORT doc._modified DESC
	LIMIT 1
	RETURN doc.CommitHash
`
const GetLastEntry = `
	FOR doc IN @@collection
	FILTER doc.RepoID == @repoID
	SORT doc._modified DESC
	LIMIT 1
	RETURN doc
`
const GetRepoInfo = `
LET RepoIDs = (
	FOR doc IN @@collection
		COLLECT RepoID = doc.RepoID 
		RETURN RepoID
	)

FOR RepoID IN RepoIDs
	LET UniqueIDs = ( 
		FOR doc IN @@collection
			FILTER doc.RepoID == RepoID
			RETURN doc
	)
	LET date_added = (
		FOR doc IN UniqueIDs
			SORT doc._modified ASC
			LIMIT 1
			RETURN doc._modified
	)
	LET result = (
		FOR doc IN UniqueIDs
			SORT doc._modified DESC
			LIMIT 1
			RETURN doc
	)
	RETURN {"repo_id" : result[0].RepoID,
	        "repo_name" : result[0].RepoName,
            "branch" : result[0].Branches,
            "external_repo_url" : result[0].RepoName,
	        "date_added" : date_added[0],
	        "last_synced" : result[0]._modified,
            "last_synced_commit_id" : result[0].CommitHash,
            "current_branch" : result[0].Branch,
	        }
`
const GetBranchID = `
FOR doc IN @@collection
	FILTER doc.RepoID == @repoID
	SORT doc._modified DESC
	LIMIT 1
	RETURN doc.BranchID
`
const GetRepoFiles = `
LET repo_name = (
	FOR doc IN @@collection
    	FILTER doc.RepoID == @repoID
    	SORT doc._modified DESC
    	LIMIT 1
    	RETURN doc.RepoName
  )

FOR doc IN @@files_collection
	RETURN {
		"file_id" : doc._id,
		"file_name": doc.Name,
		"file_path": doc.Path,
		"current_commit_time": doc.current_commit_time != null ? doc.current_commit_time : null,
		"current_commit": doc.current_commit != null ? doc.current_commit : null
	}
`
const GetFilePath = `
	FOR doc IN @@collection
	FILTER doc._id == @fileID
	LIMIT 1
	RETURN doc.Path
`

const DeleteRepoInfoDocs = `
FOR doc in @@collection
	FILTER doc.RepoID == @repoID
	REMOVE doc IN @@collection
`

const CheckFodBranch = `
FOR doc in @@collection
	FILTER doc.Branch == @branch
	LIMIT 1
	RETURN doc.BranchID
`

const GetBranchIDs = `
FOR doc IN @@collection
	FILTER doc.RepoID ==@repoID 
	COLLECT BranchID = doc.BranchID
	RETURN BranchID

`
