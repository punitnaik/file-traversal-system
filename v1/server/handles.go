package server

import (
	"errors"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/punitnaik/file-traversal-system-v1/db/arangodb"
	"gitlab.com/punitnaik/file-traversal-system-v1/fts"
	"gitlab.com/punitnaik/file-traversal-system-v1/utils"
	gitutils "gitlab.com/punitnaik/file-traversal-system-v1/utils/git"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
)

func (s *Server) Clone() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get payload from request body
		log := s.Logger.Logger
		requestBody, ok := c.Get("Payload")
		if !ok {
			log.Error().Err(errors.New("payload not found")).Msg("payload not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		user_id, ok := payload["user_id"]
		if !ok {
			log.Error().Err(errors.New("user_id not found")).Msg("user_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		repo_url, ok := payload["repo_url"].(string)
		if !ok {
			log.Error().Err(errors.New("repo_url not found")).Msg("repo_url not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		method, ok := payload["method"]
		if !ok {
			log.Error().Err(errors.New("method not found")).Msg("method not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}

		repoName := utils.ExtractRepoName(repo_url)
		path := s.Config.GetString("asvatthi.workdir") + user_id.(string) + "/"
		clonepath := path + repoName

		var repo gitutils.GitRepo
		var auth transport.AuthMethod
		var err error
		method_data := make(map[string]interface{})

		switch method {
		case "ssh":
			name, ok := payload["name"]
			if !ok {
				log.Error().Err(errors.New("name not found")).Msg("name not found")
				c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
				return
			}

			auth, err = repo.SSHBasicAuth(path + "SpecialFolder/" + name.(string) + ".pem")
			if err != nil {
				log.Error().Err(err).Msg("unable to get auth")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}

			method_data["method"] = "ssh"
			method_data["name"] = name

		case "oauth":
			access_token, ok := payload["access_token"].(string)
			if !ok {
				log.Error().Err(errors.New("access_token not found")).Msg("access_token not found")
				c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
				return
			}

			auth = repo.OauthBasicAuth(access_token)
			method_data["method"] = "oauth"

		default:
			log.Error().Err(errors.New("invalid method")).Msg("invalid method")
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid method"})
			return

		}

		err = repo.Clone(clonepath, repo_url, auth)
		if err != nil {
			log.Error().Err(err).Msg("unable to clone the repo")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// Check if the user already has a database
		exists, err := s.ArangoDB.Client.DatabaseExists(c.Request.Context(), user_id.(string))
		if err != nil {
			log.Error().Err(err).Msg("unable to check if the database exists or not")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// Set up and Load the data to arangoDB
		var arangodb arangodb.ArangoDB
		if exists {
			arangodb, err = s.ArangoDB.LoadArangoDBDatabase(c.Request.Context(), user_id.(string))
			if err != nil {
				log.Error().Err(err).Msg("unable to load arangodb database")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}
		} else {
			arangodb, err = s.ArangoDB.SetupArangoDBDatabase(c.Request.Context(), user_id.(string))
			if err != nil {
				log.Error().Err(err).Msg("unable to create arangodb database")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}
		}

		// Create a id for repo
		repo_id := utils.GetNewUUID("repo-")
		brach_id := utils.GetNewUUID("branch-")
		if err = arangodb.SetupCollections(c.Request.Context(), brach_id); err != nil {
			log.Error().Err(err).Msg("unable to create arangodb collections")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		f := fts.FileTraverser{
			RepoPath: clonepath,
			DB:       &arangodb,
			Ctx:      c.Request.Context(),
			Repo:     repo,
		}

		data, err := f.Traverse(repo_id, repo_url, brach_id, method_data)
		if err != nil {
			log.Error().Err(err).Msg("unable to traverse the repo")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully cloned the repo")
		c.JSON(http.StatusOK, data)
	}
}

func (s *Server) DeleteRepo() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get payload from request body
		log := s.Logger.Logger
		requestBody, ok := c.Get("Payload")
		if !ok {
			log.Error().Err(errors.New("payload not found")).Msg("payload not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		repoId, ok := payload["repo_id"]
		if !ok {
			log.Error().Err(errors.New("repo_id not found")).Msg("repo_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		user_id, ok := payload["user_id"]
		if !ok {
			log.Error().Err(errors.New("user_id not found")).Msg("user_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}

		// Get arangoDB database from client
		arangodb, err := s.ArangoDB.GetDatabase(c.Request.Context(), user_id.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		path := s.Config.GetString("asvatthi.workdir") + user_id.(string) + "/"
		repoName, err := arangodb.GetRepoName(c.Request.Context(), repoId.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo_name from arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		if err := os.RemoveAll(path + repoName); err != nil {
			log.Error().Err(err).Msg("Repo deletion failed due to Server error")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		branch_ids, err := arangodb.GetBranchIDs(c.Request.Context(), repoId.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get branch_id from arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		if err := arangodb.DeleteCollections(c.Request.Context(), branch_ids); err != nil {
			log.Error().Err(err).Msg("Unable delete arangodb collection")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		if err = arangodb.DeleteRepoInfoDocs(c.Request.Context(), repoId.(string)); err != nil {
			log.Error().Err(err).Msg("Unable to delete repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully deleted the repo")
		c.JSON(http.StatusOK, gin.H{"status": "successfully deleted the repo"})
	}
}

func (s *Server) Fetch() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Get payload from request body
		requestBody, ok := c.Get("Payload")
		if !ok {
			log.Error().Err(errors.New("payload not found")).Msg("payload not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		user_id, ok := payload["user_id"].(string)
		if !ok {
			log.Error().Err(errors.New("user_id not found")).Msg("user_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		repo_id, ok := payload["repo_id"].(string)
		if !ok {
			log.Error().Err(errors.New("repo_id not found")).Msg("repo_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		method, ok := payload["method"].(string)
		if !ok {
			log.Error().Err(errors.New("method not found")).Msg("method not found")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "method not found"})
			return
		}

		// Get arangoDB database from client
		arangodb, err := s.ArangoDB.GetDatabase(c.Request.Context(), user_id)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		path := s.Config.GetString("asvatthi.workdir") + user_id + "/"
		repo_info, err := arangodb.GetLastEntry(c.Request.Context(), repo_id)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo_name from arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		repo_name, ok := repo_info["RepoName"].(string)
		if !ok {
			log.Error().Err(errors.New("repo_name not found")).Msg("repo_name not found")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "repo_name not found"})
			return
		}
		repoPath := path + repo_name

		// Open the repo
		var repo gitutils.GitRepo
		if err := repo.OpenRepo(repoPath); err != nil {
			log.Error().Err(err).Msg("unable to open the repo")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		var auth transport.AuthMethod
		switch method {
		case "ssh":
			clone_info, ok := repo_info["CloneInfo"].(map[string]interface{})
			if !ok {
				log.Error().Err(errors.New("clone_info not found")).Msg("clone_info not found")
				c.JSON(http.StatusInternalServerError, gin.H{"error": "clone_info not found"})
				return
			}
			name, ok := clone_info["name"]
			if !ok {
				log.Error().Err(errors.New("name not found")).Msg("name not found")
				c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
				return
			}

			auth, err = repo.SSHBasicAuth(path + "SpecialFolder/" + name.(string) + ".pem")
			if err != nil {
				log.Error().Err(err).Msg("unable to get auth")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}

		case "oauth":
			access_token, ok := payload["access_token"].(string)
			if !ok {
				log.Error().Err(errors.New("access_token not found")).Msg("access_token not found")
				c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
				return
			}

			auth = repo.OauthBasicAuth(access_token)

		default:
			log.Error().Err(errors.New("invalid method")).Msg("invalid method")
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid method"})
			return

		}

		// Fetch
		if err := repo.Pull(auth); err != nil {
			if err != gitutils.NoErrAlreadyUpToDate {
				log.Error().Err(err).Msg("unable to fetch the changes")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}
			log.Info().Msg(err.Error())
			c.JSON(http.StatusOK, gin.H{"status": err.Error()})
			return
		}

		f := fts.FileTraverser{
			RepoPath: repoPath,
			DB:       &arangodb,
			Ctx:      c.Request.Context(),
			Repo:     repo,
		}

		data, err := f.Sync(repo_id)
		if err != nil {
			log.Error().Err(err).Msg("unable to sync the repo")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully synced the repo")
		c.JSON(http.StatusOK, data)
	}
}

func (s *Server) GetRepoInfos() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get payload from request body
		log := s.Logger.Logger
		requestBody, ok := c.Get("Payload")
		if !ok {
			log.Error().Err(errors.New("payload not found")).Msg("payload not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		user_id, ok := payload["user_id"]
		if !ok {
			log.Error().Err(errors.New("user_id not found")).Msg("user_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}

		// Get arangoDB database from client
		arangodb, err := s.ArangoDB.GetDatabase(c.Request.Context(), user_id.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		repos, err := arangodb.GetRepoInfo(c.Request.Context())
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned repo info")
		c.JSON(http.StatusOK, gin.H{"repositories": repos})
	}
}

func (s *Server) GetRepoFiles() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get payload from request body
		log := s.Logger.Logger
		requestBody, ok := c.Get("Payload")
		if !ok {
			log.Error().Err(errors.New("payload not found")).Msg("payload not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		user_id, ok := payload["user_id"]
		if !ok {
			log.Error().Err(errors.New("user_id not found")).Msg("user_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		repoId, ok := payload["repo_id"]
		if !ok {
			log.Error().Err(errors.New("repo_id not found")).Msg("repo_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}

		// Get arangoDB database from client
		arangodb, err := s.ArangoDB.GetDatabase(c.Request.Context(), user_id.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		branch_id, err := arangodb.GetBranchID(c.Request.Context(), repoId.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get branch_id")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		files, err := arangodb.GetRepoFiles(c.Request.Context(), repoId.(string), branch_id)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo files")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned repo files")
		c.JSON(http.StatusOK, gin.H{"files": files})
	}
}

func (s *Server) GetFileContent() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get payload from request body
		log := s.Logger.Logger
		requestBody, ok := c.Get("Payload")
		if !ok {
			log.Error().Err(errors.New("payload not found")).Msg("payload not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		user_id, ok := payload["user_id"]
		if !ok {
			log.Error().Err(errors.New("user_id not found")).Msg("user_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		repoId, ok := payload["repo_id"]
		if !ok {
			log.Error().Err(errors.New("repo_id not found")).Msg("repo_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		file_id, ok := payload["file_id"]
		if !ok {
			log.Error().Err(errors.New("file_id not found")).Msg("file_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}

		// Get arangoDB database from client
		arangodb, err := s.ArangoDB.GetDatabase(c.Request.Context(), user_id.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		repoInfo, err := arangodb.GetLastEntry(c.Request.Context(), repoId.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		filepath, err := arangodb.GetFilePath(c.Request.Context(), file_id.(string), repoInfo["BranchID"].(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get file path")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		abspath := s.Config.GetString("asvatthi.workdir") + user_id.(string) + "/" + repoInfo["RepoName"].(string) + "/" + filepath
		content, err := fts.ReadFileContent(abspath)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get file content")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned file content")
		c.String(http.StatusOK, content)
	}
}

func (s *Server) Checkout() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get payload from request body
		log := s.Logger.Logger
		requestBody, ok := c.Get("Payload")
		if !ok {
			log.Error().Err(errors.New("payload not found")).Msg("payload not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		user_id, ok := payload["user_id"]
		if !ok {
			log.Error().Err(errors.New("user_id not found")).Msg("user_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		repoId, ok := payload["repo_id"].(string)
		if !ok {
			log.Error().Err(errors.New("repo_id not found")).Msg("repo_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		branch_name, ok := payload["branch_name"]
		if !ok {
			log.Error().Err(errors.New("branch_name not found")).Msg("branch_name not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}

		// Get arangoDB database from client
		arangodb, err := s.ArangoDB.GetDatabase(c.Request.Context(), user_id.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		path := s.Config.GetString("asvatthi.workdir") + user_id.(string) + "/"
		repo_info, err := arangodb.GetLastEntry(c.Request.Context(), repoId)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo info from arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		repo_name, ok := repo_info["RepoName"].(string)
		if !ok {
			log.Error().Err(errors.New("repo_name not found")).Msg("repo_name not found")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "repo_name not found"})
			return
		}
		repo_url, ok := repo_info["RepoURL"].(string)
		if !ok {
			log.Error().Err(errors.New("repo_url not found")).Msg("repo_url not found")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "repo_url not found"})
			return
		}
		last_synced, ok := repo_info["_modified"].(string)
		if !ok {
			log.Error().Err(errors.New("last_synced not found")).Msg("last_synced not found")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "last_synced not found"})
			return
		}
		clone_info, ok := repo_info["CloneInfo"]
		if !ok {
			log.Error().Err(errors.New("clone_info not found")).Msg("clone_info not found")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "clone_info not found"})
			return
		}

		var livedocs_array []string
		_, ok = repo_info["LivedocsBranches"]
		if !ok {
			log.Error().Err(errors.New("livedocs_array not found")).Msg("livedocs_array not found")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "livedocs_array not found"})
			return
		} else {
			for _, branch := range repo_info["LivedocsBranches"].([]interface{}) {
				livedocs_array = append(livedocs_array, branch.(string))
			}
		}

		// Open the repo
		var repo gitutils.GitRepo
		repoPath := path + repo_name
		if err := repo.OpenRepo(repoPath); err != nil {
			log.Error().Err(err).Msg("unable to open the repo")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		if err := repo.Checkout(branch_name.(string)); err != nil {
			log.Error().Err(err).Msg("unable to checkout")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// check if the data is already in arangodb
		branch_id, exists, err := arangodb.CheckFodBranch(c.Request.Context(), branch_name.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo info from arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		f := fts.FileTraverser{
			RepoPath: repoPath,
			DB:       &arangodb,
			Ctx:      c.Request.Context(),
			Repo:     repo,
		}
		var data map[string]interface{}
		if exists {
			data, err = f.UpdateDatabaseInfo(repoId, repo_url, branch_id, last_synced, livedocs_array, clone_info)
			if err != nil {
				log.Error().Err(err).Msg("unable to update the repo info")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			}
		} else {
			branch_id = utils.GetNewUUID("branch-")
			if err = arangodb.SetupCollections(c.Request.Context(), branch_id); err != nil {
				log.Error().Err(err).Msg("unable to create arangodb collections")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}

			data, err = f.Traverse(repoId, repo_url, branch_id, clone_info)
			if err != nil {
				log.Error().Err(err).Msg("unable to traverse the repo")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}
		}

		log.Info().Msg("successfully switched branch")
		data["status"] = "successfully switched branch"
		c.JSON(http.StatusOK, data)
	}
}

func (s *Server) GetRepoInfo() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get payload from request body
		log := s.Logger.Logger
		repo_id := c.Param("repo_id")
		requestBody, ok := c.Get("Payload")
		if !ok {
			log.Error().Err(errors.New("payload not found")).Msg("payload not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		user_id, ok := payload["user_id"]
		if !ok {
			log.Error().Err(errors.New("user_id not found")).Msg("user_id not found")
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}

		// Get arangoDB database from client
		arangodb, err := s.ArangoDB.GetDatabase(c.Request.Context(), user_id.(string))
		if err != nil {
			log.Error().Err(err).Msg("Unable to get arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		repo_info, err := arangodb.GetLastEntry(c.Request.Context(), repo_id)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned repo info")
		c.JSON(http.StatusOK, repo_info)
	}
}
