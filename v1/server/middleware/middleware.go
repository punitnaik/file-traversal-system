package middleware

import (
	"errors"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	logger "gitlab.com/punitnaik/file-traversal-system-v1/log"
)

// Authorize is a middleware function for session management using JWT
func Authorize(log *logger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {

		// Get the username and password from environment variables
		FTS_USER := os.Getenv("FTS_USER")
		FTS_PASSWD := os.Getenv("FTS_PASSWD")

		// Check if both environment variables are set
		if FTS_USER == "" || FTS_PASSWD == "" {
			log.Logger.Error().Err(errors.New("Username or Password is not set")).Msg("Username or Password is not set")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Username or Password is not set"})
			return
		}

		username, password, ok := c.Request.BasicAuth()
		if !ok {
			log.Logger.Error().Err(errors.New("No Basic Auth credentials provided")).Msg("No Basic Auth credentials provided")
			c.JSON(http.StatusUnauthorized, gin.H{"error": "No Basic Auth credentials provided"})
			return
		}

		if FTS_USER != username || FTS_PASSWD != password {
			// Compare it with the request data
			log.Logger.Error().Err(errors.New("Username or Password does not match")).Msg("Username or Password does not match")
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Username or Password does not match"})
			return
		}

		// Get the JSON from the request body
		var requestBody interface{}
		if err := c.ShouldBindJSON(&requestBody); err != nil {
			log.Logger.Error().Err(err).Msg("failed to bind json")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		payload := requestBody.(map[string]interface{})

		// Set user ID in the Gin context
		log.Logger.Info().Msg("Payload: " + log.String(payload))
		c.Set("Payload", payload)
		c.Next()
	}
}
