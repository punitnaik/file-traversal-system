package logger

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
	"gitlab.com/punitnaik/file-traversal-system-v1/utils"
	"gopkg.in/natefinch/lumberjack.v2"
)

type Logger struct {
	Logger    zerolog.Logger
	GinWriter lumberjack.Logger
	FTSWriter lumberjack.Logger
}

func (log *Logger) Start(config *utils.Config) {
	subConfig := config.Sub("log.fts")
	log.FTSWriter = lumberjack.Logger{
		Filename:   subConfig.GetString("path") + subConfig.GetString("filename"),
		MaxSize:    subConfig.GetInt("MaxSize"),
		MaxBackups: subConfig.GetInt("MaxBackups"),
		MaxAge:     subConfig.GetInt("MaxAge"),
		Compress:   true,
	}

	subConfig = config.Sub("log.gin")
	log.GinWriter = lumberjack.Logger{
		Filename:   subConfig.GetString("path") + subConfig.GetString("filename"),
		MaxSize:    subConfig.GetInt("MaxSize"),
		MaxBackups: subConfig.GetInt("MaxBackups"),
		MaxAge:     subConfig.GetInt("MaxAge"),
		Compress:   true,
	}

	// Use Zerolog with a console writer and a file writer.
	multi := zerolog.MultiLevelWriter(zerolog.ConsoleWriter{Out: os.Stdout}, &log.FTSWriter)
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	log.Logger = zerolog.New(multi).With().Timestamp().Logger()
}

func (log *Logger) Close() {
	log.FTSWriter.Close()
	log.GinWriter.Close()
}

func (log *Logger) String(input any) string {
	return fmt.Sprintf("%+v", input)
}
