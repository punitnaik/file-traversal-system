package fts

import (
	"context"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/arangodb/go-driver"
	"gitlab.com/punitnaik/file-traversal-system-v1/db/arangodb"
	"gitlab.com/punitnaik/file-traversal-system-v1/utils"
	gitutils "gitlab.com/punitnaik/file-traversal-system-v1/utils/git"
)

type FileTraverser struct {
	RepoPath string
	DB       *arangodb.ArangoDB
	Ctx      context.Context
	Repo     gitutils.GitRepo
}

type RepoInfo struct {
	CommitHash       string    `json:"commit_id"`
	LastSynced       string    `json:"last_synced"`
	RepoName         string    `json:"repo_name"`
	RepoID           string    `json:"repo_id"`
	Branches         []string  `json:"branch_names"`
	DateAdded        string    `json:"date_added"`
	BranchID         string    `json:"brach_id"`
	Branch           string    `json:"branch"`
	RepoURL          string    `json:"repo_url"`
	LivedocsBranches []string  `json:"livedocs_array"`
	CloneInfo        CloneInfo `json:"clone_info"`
}

type CloneInfo struct {
	Method  string `json:"method"`
	KeyName string `json:"key_name"`
}

func (fts *FileTraverser) GetFileInfo(path string) (fs.FileInfo, error) {
	file, err := os.Stat(fts.RepoPath + "/" + path)
	if err != nil {
		return nil, err
	}

	return file, nil
}

func (fts *FileTraverser) Open(path string) (*os.File, error) {
	dir, err := os.Open(fts.RepoPath + "/" + path)
	if err != nil {
		return nil, err
	}

	return dir, nil
}

func (fts *FileTraverser) Traverse(repo_id, repo_url, brach_id string, clone_info interface{}) (map[string]interface{}, error) {
	if err := fts.RecursiveTraverse("", driver.DocumentMeta{}); err != nil {
		return nil, err
	}

	data, err := fts.UpdateDatabaseInfo(repo_id, repo_url, brach_id, "", nil, clone_info)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (fts *FileTraverser) RecursiveTraverse(path string, parent driver.DocumentMeta) error {
	file, err := fts.GetFileInfo(path)
	if err != nil {
		return err
	}

	Filedata := map[string]interface{}{
		"Name": file.Name(),
		"Path": path,
	}

	if !file.IsDir() {
		extension := filepath.Ext(file.Name())
		// gitInfo, err := fts.Repo.GetGitRelatedInfo(path)
		// if err != nil {
		// 	Filedata["CommitHash"] = nil
		// 	Filedata["Modified"] = nil
		// } else {
		// 	Filedata["CommitHash"] = gitInfo.Hash
		// 	Filedata["Modified"] = gitInfo.Committer.When
		// }
		Filedata["Extension"] = extension

		if _, err = AddEntry(fts.Ctx, Filedata, parent, fts.DB.Files, fts.DB.Contains); err != nil {
			return err
		}

		return nil
	}

	FoldDoc, err := AddEntry(fts.Ctx, Filedata, parent, fts.DB.Folders, fts.DB.Contains)
	if err != nil {
		return err
	}

	// Open the directory
	dir, err := fts.Open(path)
	if err != nil {
		return err
	}
	defer dir.Close()

	// Read the directory contents
	files, err := dir.Readdir(-1)
	if err != nil {
		return err
	}
	for _, file := range files {
		// filePath := path + "/" + file.Name()
		var filePath string
		if file.Name() == ".git" {
			continue
		}
		if path != "" {
			filePath = path + "/" + file.Name()
		} else {
			filePath = file.Name()
		}
		if err = fts.RecursiveTraverse(filePath, FoldDoc); err != nil {
			return err
		}
	}

	return nil
}

func AddEntry(ctx context.Context, data map[string]interface{}, parent driver.DocumentMeta, dir, Contains driver.Collection) (driver.DocumentMeta, error) {
	FileDoc, err := arangodb.CreateArangoDBDocument(ctx, dir, data)
	if err != nil {
		return driver.DocumentMeta{}, err
	}

	if !parent.ID.IsEmpty() {
		_, err = arangodb.CreateArangoDBEdge(ctx, Contains, string(parent.ID), string(FileDoc.ID))
		if err != nil {
			return driver.DocumentMeta{}, err
		}
	}

	return FileDoc, err
}

func (fts *FileTraverser) UpdateDatabaseInfo(repo_id, repo_url, brach_id, last_synced string, livedocs_array []string, clone_info interface{}) (map[string]interface{}, error) {
	// Get Commit details
	commitID, err := fts.Repo.GetCommitHash()
	if err != nil {
		return nil, err
	}

	branchNames, err := fts.Repo.Branches()
	if err != nil {
		return nil, err
	}

	branch, err := fts.Repo.Branch()
	if err != nil {
		return nil, err
	}

	date_added := utils.GetUTCTimestamp()
	if last_synced == "" {
		last_synced = date_added
	}

	if livedocs_array == nil {
		livedocs_array = []string{branch}
	} else {
		livedocs_array = append(livedocs_array, branch)
	}

	repo_name := utils.ExtractRepoName(repo_url)
	RepoInfo := map[string]interface{}{
		"CommitHash":       commitID.String(),
		"_modified":        last_synced,
		"RepoName":         repo_name,
		"RepoID":           repo_id,
		"Branches":         branchNames,
		"DateAdded":        date_added,
		"BranchID":         brach_id,
		"Branch":           branch,
		"RepoURL":          repo_url,
		"LivedocsBranches": livedocs_array,
		"CloneInfo":        clone_info,
	}

	// Update the details in arangodb
	Info, err := fts.DB.Database.Collection(fts.Ctx, "Info")
	if err != nil {
		return nil, err
	}

	_, err = Info.CreateDocument(fts.Ctx, RepoInfo)
	if err != nil {
		return nil, err
	}

	return_data := map[string]interface{}{
		"external_repo_url":     repo_url,
		"date_added":            date_added,
		"last_synced":           last_synced,
		"last_synced_commit_id": commitID.String(),
		"current_branch":        branch,
		"repo_name":             repo_name,
		"repo_id":               repo_id,
		"branch":                branchNames,
	}
	return return_data, nil
}

func (fts *FileTraverser) Sync(repo_id string) (map[string]interface{}, error) {
	// Get last commit id from the database
	lastEntry, err := fts.DB.GetLastEntry(fts.Ctx, repo_id)
	if err != nil {
		return nil, err
	}

	lastCommit, ok := lastEntry["CommitHash"]
	if !ok {
		return nil, err
	}

	brach_id, ok := lastEntry["BranchID"].(string)
	if !ok {
		return nil, err
	}

	repo_url, ok := lastEntry["RepoURL"].(string)
	if !ok {
		return nil, err
	}

	last_synced, ok := lastEntry["_modified"].(string)
	if !ok {
		return nil, err
	}

	clone_info, ok := lastEntry["CloneInfo"].(map[string]interface{})
	if !ok {
		return nil, err
	}

	var livedocs_array []string
	_, ok = lastEntry["LivedocsBranches"]
	if !ok {
		return nil, err
	} else {
		for _, branch := range lastEntry["LivedocsBranches"].([]interface{}) {
			livedocs_array = append(livedocs_array, branch.(string))
		}
	}

	// Fetch the Changes
	changes, err := fts.GetFetchChanges(lastCommit.(string))
	if err != nil {
		return nil, err
	}

	for _, change := range changes {
		action, err := change.Action()
		if err != nil {
			return nil, err
		}
		switch action.String() {
		case "Insert":
			if err = fts.HandleInsertAction(change.To.Name, brach_id); err != nil {
				return nil, err
			}
		case "Delete":
			if err = fts.HandleDeleteAction(change.From.Name, brach_id); err != nil {
				return nil, err
			}
		case "Modify":
			continue
		default:
			return nil, err
		}
	}

	data, err := fts.UpdateDatabaseInfo(repo_id, repo_url, brach_id, last_synced, livedocs_array, clone_info)
	if err != nil {
		return nil, err
	}

	return data, nil
}

// Function to read the content of a file
func ReadFileContent(filepath string) (string, error) {
	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		return "", err
	}

	data, err := os.ReadFile(filepath)
	if err != nil {
		return "", err
	}
	return string(data), nil
}
