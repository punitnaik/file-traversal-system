package utils

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	*viper.Viper
}

func (v *Config) Init() error {
	// Get a list of all files
	folderPath := ".config/asvatthi"
	// folderPath := "config"
	files, err := GetYAMLFiles(folderPath)
	if err != nil {
		return err
	}

	v.Viper = viper.New()
	// Set the path to the folder containing the configuration files
	v.AddConfigPath(folderPath)

	// Iterate through each file and load its configuration
	for _, file := range files {
		// Set the configuration file name (without extension)
		v.SetConfigName(strings.TrimSuffix(file.Name(), filepath.Ext(file.Name())))

		// Read in the configuration file
		err := v.MergeInConfig()
		if err != nil {
			return err
		}
	}

	return nil
}

func GetYAMLFiles(foderPath string) ([]os.FileInfo, error) {
	files, err := os.ReadDir(foderPath)
	if err != nil {
		return nil, err
	}
	var yamlFiles []os.FileInfo
	for _, file := range files {
		if strings.HasSuffix(file.Name(), ".yaml") || strings.HasSuffix(file.Name(), ".yml") {
			fileInfo, err := file.Info()
			if err != nil {
				return nil, err
			}
			yamlFiles = append(yamlFiles, fileInfo)
		}
	}
	return yamlFiles, nil
}
