package utils

import (
	"errors"
	"math/rand"
	"os"
	"path/filepath"
	"time"

	"github.com/google/uuid"
)

type ExitCode struct {
	Code int
	Err  error
	Msg  string
}

func GetUTCTimestamp() string {
	// Get the current time in UTC
	return time.Now().UTC().Format("2006-01-02T15:04:05.999999999Z07:00")
}

func GenerateRandomString(size int, strType string) string {
	source := rand.NewSource(time.Now().UnixNano())
	rng := rand.New(source)

	const (
		numericChars      = "0123456789"
		alphabetChars     = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		alphanumericChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	)
	var chars string

	switch strType {
	case "numeric":
		chars = numericChars
	case "alphabet":
		chars = alphabetChars
	case "alphanumeric":
		chars = alphanumericChars
	default:
		return ""
	}

	result := make([]byte, size)
	for i := 0; i < size; i++ {
		result[i] = chars[rng.Intn(len(chars))]
	}
	return string(result)
}

func CreateFolder(folderPath string) error {
	// Check if the folder already exists
	if _, err := os.Stat(folderPath); os.IsNotExist(err) {
		// Create the folder
		err := os.Mkdir(folderPath, os.ModePerm)
		if err != nil {
			return err
		}
	} else {
		return errors.New("Folder " + folderPath + " already exists")
	}

	return nil
}

func GetNewUUID(prefix string) string {
	return prefix + uuid.New().String()
}

// extractRepoName extracts the repository name from the Git URL
func ExtractRepoName(url string) string {
	// Assuming the URL is in the format "git@github.com:username/repo.git"
	// Extracting "repo" from the URL
	base := filepath.Base(url)
	return base[:len(base)-4] // Removing ".git" from the end
}

func GetFolderPath(filePath string) string {
	// Using filepath.Dir to get the directory path
	dirPath := filepath.Dir(filePath)
	if dirPath == "." {
		return ""
	}
	return dirPath
}
