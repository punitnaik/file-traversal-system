package utils

import (
	"sync"
)

// WorkerWaitGroup is a wrapper around sync.WaitGroup that provides
// additional functionality to keep track of the number of tasks.
type WorkerWaitGroup struct {
	Wg      sync.WaitGroup
	counter int
	mu      sync.Mutex
}

// Add adds delta to the WaitGroup counter and updates the internal counter.
func (w *WorkerWaitGroup) Add(delta int) {
	w.mu.Lock()
	defer w.mu.Unlock()

	w.Wg.Add(delta)
	w.counter += delta
}

// Done decrements the WaitGroup counter and updates the internal counter.
func (w *WorkerWaitGroup) Done() {
	w.mu.Lock()
	defer w.mu.Unlock()

	w.Wg.Done()
	w.counter--
}

// Wait waits for the WaitGroup counter to reach zero.
func (w *WorkerWaitGroup) Wait() {
	w.Wg.Wait()
}

// Count returns the current value of the internal counter.
func (w *WorkerWaitGroup) Count() int {
	w.mu.Lock()
	defer w.mu.Unlock()

	return w.counter
}

type WorkRequest struct {
	Function func(args ...interface{})
	Args     []interface{}
}

type WorkerPool struct {
	minWorkers int
	maxWorkers int
	taskQueue  chan WorkRequest
	Wg         WorkerWaitGroup
	mu         sync.Mutex
}

func NewWorkerPool(minWorkers, maxWorkers, bufferSize int) *WorkerPool {
	pool := &WorkerPool{
		minWorkers: minWorkers,
		maxWorkers: maxWorkers,
		taskQueue:  make(chan WorkRequest, bufferSize),
	}

	// Start minimum number of workers
	for i := 0; i < minWorkers; i++ {
		pool.startWorker()
	}

	return pool
}

func (wp *WorkerPool) startWorker() {
	wp.Wg.Add(1)

	go func() {
		defer wp.Done()
		count := 1
		for task := range wp.taskQueue {
			count++
			task.Function(task.Args...)

		}
	}()
}

func (wp *WorkerPool) startExtraWorker() {
	wp.Wg.Add(1)

	go func() {
		defer wp.Done()

		task, ok := <-wp.taskQueue
		if ok {
			task.Function(task.Args...)
		}

	}()
}

func (wp *WorkerPool) Done() {
	wp.mu.Lock()
	defer wp.mu.Unlock()
	wp.Wg.Done()

	if wp.Wg.Count() < wp.minWorkers {
		wp.startWorker()
	}
}

func (wp *WorkerPool) SubmitTask(task WorkRequest) {
	wp.mu.Lock()
	defer wp.mu.Unlock()

	count := wp.Wg.Count()
	if count < wp.maxWorkers { // && count >= wp.minWorkers
		wp.startExtraWorker()
	}

	wp.taskQueue <- task
}

func (wp *WorkerPool) Wait() {
	close(wp.taskQueue)
	wp.Wg.Wait()
}
