package main

import (
	"gitlab.com/punitnaik/file-traversal-system-v1/db/arangodb"
	logger "gitlab.com/punitnaik/file-traversal-system-v1/log"
	"gitlab.com/punitnaik/file-traversal-system-v1/server"
	"gitlab.com/punitnaik/file-traversal-system-v1/utils"
)

func main() {
	// Config
	var Config utils.Config
	err := Config.Init()
	if err != nil {
		panic(err)
	}

	// Logger
	var Logger logger.Logger
	Logger.Start(&Config)
	defer Logger.Close()

	// ArangoDB
	var ArangoDB arangodb.ArangoDBClient
	err = ArangoDB.Init(&Config)
	if err != nil {
		panic(err)
	}

	s := &server.Server{
		ArangoDB: &ArangoDB,
		Logger:   &Logger,
		Config:   &Config,
	}
	s.Start()
}
