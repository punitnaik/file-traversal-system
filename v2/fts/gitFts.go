package fts

import (
	"context"
	"fmt"

	driver "github.com/arangodb/go-driver"
	"gitlab.com/punitnaik/file-traversal-system-v2/db/arangodb"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

func GetDiffTree(start, end *object.Commit) (object.Changes, error) {
	// Get the tree of each commit
	tree1, err := end.Tree()
	if err != nil {
		return nil, err
	}

	tree2, err := start.Tree()
	if err != nil {
		return nil, err
	}

	// Get the changes between the two trees
	changes, err := object.DiffTree(tree1, tree2)
	if err != nil {
		return nil, err
	}

	return changes, nil
}

func (fts *FileTraverser) GetFetchChanges(commitID, branch string) (object.Changes, error) {
	lastCommit, err := fts.Repo.GetCommitObjectFromID(plumbing.NewHash(commitID))
	if err != nil {
		return nil, err
	}

	ref, err := fts.Repo.Reference(plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", branch)), true)
	if err != nil {
		return nil, err
	}

	latestCommit, err := fts.Repo.CommitObject(ref.Hash())
	if err != nil {
		return nil, err
	}

	changes, err := GetDiffTree(latestCommit, lastCommit)
	if err != nil {
		return nil, err
	}

	return changes, nil
}

func (fts *FileTraverser) HandleInsertAction(ctx context.Context, path string, Files driver.Collection) error {
	file_info := GetFileInfo(path)
	_, err := Files.CreateDocument(ctx, file_info)
	if err != nil {
		return err
	}

	return nil
}

func (fts *FileTraverser) HandleDeleteAction(ctx context.Context, path, branch_id string, Files driver.Collection) error {
	// Get the file document
	file_info := GetFileInfo(path)
	cursor, err := fts.DB.Database.Query(ctx, arangodb.GetDocFromFileInfo, map[string]interface{}{
		"@collection": branch_id + arangodb.FilesSuffix,
		"name":        file_info.Name,
		"file_path":   file_info.FilePath,
		"parent_path": file_info.ParentPath,
	})
	if err != nil {
		return err
	}

	// Read the result
	var file arangodb.DocumentMeta
	_, err = cursor.ReadDocument(ctx, &file.DocumentMeta)
	if err != nil {
		return err
	}
	cursor.Close()

	// Delete both Documents
	_, err = Files.RemoveDocument(ctx, file.Key)
	if err != nil {
		return err
	}

	defer cursor.Close()
	return nil
}

func HandleModifyAction(ctx context.Context, db *arangodb.ArangoDB, path string) error {
	return nil
}
