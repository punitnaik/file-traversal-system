package fts

import (
	"context"
	"os"
	"path/filepath"

	"gitlab.com/punitnaik/file-traversal-system-v2/db/arangodb"
	"gitlab.com/punitnaik/file-traversal-system-v2/utils"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

type FileTraverser struct {
	DB   arangodb.ArangoDB
	Repo GitRepo
}

type RepoInfo struct {
	CommitID         string    `json:"commit_id"`
	LastSynced       string    `json:"last_synced"`
	RepoName         string    `json:"repo_name"`
	RepoID           string    `json:"repo_id"`
	Branches         []string  `json:"branch_names"`
	DateAdded        string    `json:"date_added"`
	BranchID         string    `json:"branch_id"`
	Branch           string    `json:"branch"`
	RepoURL          string    `json:"repo_url"`
	LivedocsBranches []string  `json:"livedocs_array"`
	CloneInfo        CloneInfo `json:"clone_info"`
}

type CloneInfo struct {
	Method  string `json:"method"`
	KeyName string `json:"key_name"`
}

type FileInfo struct {
	Name       string `json:"name"`
	FilePath   string `json:"file_path"`
	ParentPath string `json:"parent_path"`
	Extension  string `json:"extension"`
}

func (fts *FileTraverser) UpdateDatabaseInfo(Ctx context.Context, RepoInfo RepoInfo) (map[string]interface{}, error) {
	// Get Commit details
	var err error
	RepoInfo.CommitID, err = fts.Repo.GetCommitID()
	if err != nil {
		return nil, err
	}

	RepoInfo.Branches, err = fts.Repo.Branches()
	if err != nil {
		return nil, err
	}

	RepoInfo.Branch, err = fts.Repo.Branch()
	if err != nil {
		return nil, err
	}

	RepoInfo.LastSynced = utils.GetUTCTimestamp()
	if RepoInfo.DateAdded == "" {
		RepoInfo.DateAdded = RepoInfo.LastSynced
	}

	if RepoInfo.LivedocsBranches == nil || len(RepoInfo.LivedocsBranches) == 0 {
		RepoInfo.LivedocsBranches = []string{RepoInfo.Branch}
	}

	// Update the details in arangodb
	Info, err := fts.DB.LoadCollection(Ctx, arangodb.InfoCollectionName)
	if err != nil {
		return nil, err
	}

	_, err = Info.CreateDocument(Ctx, RepoInfo)
	if err != nil {
		return nil, err
	}

	return_data := map[string]interface{}{
		"external_repo_url":     RepoInfo.RepoURL,
		"date_added":            RepoInfo.DateAdded,
		"last_synced":           RepoInfo.LastSynced,
		"last_synced_commit_id": RepoInfo.CommitID,
		"current_branch":        RepoInfo.Branch,
		"repo_name":             RepoInfo.RepoName,
		"repo_id":               RepoInfo.RepoID,
		"branch":                RepoInfo.Branches,
	}
	return return_data, nil
}

func (fts *FileTraverser) Sync(Ctx context.Context, RepoInfo RepoInfo) error {
	// Get branch_id for all the livedocs branches
	var branch_ids map[string]string
	if err := fts.DB.GetBranchIDFromBranch(Ctx, RepoInfo.RepoID, &branch_ids); err != nil {
		return err
	}

	// Iterate over all the livedocs branches
	for _, branch := range RepoInfo.LivedocsBranches {
		// Fetch the Changes
		changes, err := fts.GetFetchChanges(RepoInfo.CommitID, branch)
		if err != nil {
			return err
		}

		branch_id := branch_ids[branch]
		collection, err := fts.DB.LoadCollection(Ctx, branch_id+arangodb.FilesSuffix)
		if err != nil {
			return err
		}

		for _, change := range changes {
			action, err := change.Action()
			if err != nil {
				return err
			}
			switch action.String() {
			case "Insert":
				if err = fts.HandleInsertAction(Ctx, change.To.Name, collection); err != nil {
					return err
				}
			case "Delete":
				if err = fts.HandleDeleteAction(Ctx, change.From.Name, branch_id, collection); err != nil {
					return err
				}
			case "Modify":
				continue
			default:
				return err
			}
		}
	}

	return nil
}

// Function to read the content of a file
func ReadFileContent(filepath string) (string, error) {
	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		return "", err
	}

	data, err := os.ReadFile(filepath)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func (fts *FileTraverser) Traverse(ctx context.Context, branch_id string) error {
	Files, err := fts.DB.SetupCollections(ctx, branch_id)
	if err != nil {
		return err
	}

	// Get the HEAD reference
	ref, err := fts.Repo.Head()
	if err != nil {
		return err
	}

	// Get the commit object for the HEAD reference
	commit, err := fts.Repo.CommitObject(ref.Hash())
	if err != nil {
		return err
	}

	// Get the tree object for the commit
	tree, err := commit.Tree()
	if err != nil {
		return err
	}

	// Traverse the tree and print the file paths
	var FileInfoSlice []FileInfo
	err = tree.Files().ForEach(func(f *object.File) error {
		file_info := GetFileInfo(f.Name)
		FileInfoSlice = append(FileInfoSlice, file_info)
		return nil
	})

	if err != nil {
		return err
	}

	_, _, err = Files.CreateDocuments(ctx, FileInfoSlice)
	if err != nil {
		return err
	}

	return nil
}

func GetFileInfo(path string) FileInfo {
	file_name := utils.GetFileName(path)
	folder_path := utils.GetFolderPath(path)
	ext := filepath.Ext(file_name)
	return FileInfo{
		FilePath:   path,
		ParentPath: folder_path,
		Extension:  ext,
		Name:       file_name,
	}
}
