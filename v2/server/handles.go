package server

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/punitnaik/file-traversal-system-v2/db/arangodb"
	"gitlab.com/punitnaik/file-traversal-system-v2/fts"
	"gitlab.com/punitnaik/file-traversal-system-v2/utils"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
)

func (s *Server) Clone() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID  string `json:"user_id" binding:"required"`
			RepoURL string `json:"repo_url" binding:"required"`
			Method  string `json:"method"`
			Auth    string `json:"auth"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		log.Info().Msg("Payload: " + s.Logger.String(Payload))

		repo_name := utils.ExtractRepoName(Payload.RepoURL)
		path := fmt.Sprintf("%v/%v", s.Config.GetString("asvatthi.workdir"), Payload.UserID)

		var FileTraverser fts.FileTraverser
		var auth transport.AuthMethod
		var err error
		var method_data fts.CloneInfo

		switch Payload.Method {
		case "ssh":
			key_path := fmt.Sprintf("%v%v/%v.pem", path, s.Config.GetString("asvatthi.SSH-Keys"), Payload.Auth)
			auth, err = fts.SSHBasicAuth(key_path)
			if err != nil {
				log.Error().Err(err).Msg("unable to get auth")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}

			method_data.Method = Payload.Method
			method_data.KeyName = Payload.Auth

		case "oauth":
			auth = fts.OauthBasicAuth(Payload.Auth)
			method_data.Method = Payload.Method

		default:
			log.Error().Err(errors.New("invalid method")).Msg("invalid method")
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid method"})
			return

		}

		repo_id := utils.GetNewUUID(utils.RepoIDPrefix)
		clonepath := fmt.Sprintf("%v/%v/%v", path, repo_id, repo_name)
		if err = FileTraverser.Repo.Clone(clonepath, Payload.RepoURL, auth); err != nil {
			log.Error().Err(err).Msg("unable to clone the repo")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// Set up and Load the data to arangoDB
		if err = s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &FileTraverser.DB); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		brach_id := utils.GetNewUUID(utils.BranchIDPrefix)
		if err = FileTraverser.Traverse(c.Request.Context(), brach_id); err != nil {
			log.Error().Err(err).Msg("unable to traverse the repo")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		data, err := FileTraverser.UpdateDatabaseInfo(c.Request.Context(), fts.RepoInfo{
			RepoName:  repo_name,
			RepoID:    repo_id,
			RepoURL:   Payload.RepoURL,
			BranchID:  brach_id,
			CloneInfo: method_data,
		})
		if err != nil {
			log.Error().Err(err).Msg("unable to update the repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully cloned the repo")
		c.JSON(http.StatusOK, data)
	}
}

func (s *Server) Fetch() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
			Method string `json:"method"`
			Auth   string `json:"auth"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		log.Info().Msg("Payload: " + s.Logger.String(Payload))

		// Get arangoDB database from client
		var FileTraverser fts.FileTraverser
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &FileTraverser.DB); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		var RepoInfo fts.RepoInfo
		err := FileTraverser.DB.GetLastEntry(c.Request.Context(), Payload.RepoID, &RepoInfo)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo_name from arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		path := fmt.Sprintf("%v/%v/", s.Config.GetString("asvatthi.workdir"), Payload.UserID)
		repo_path := fmt.Sprintf("%v/%v/%v", path, RepoInfo.RepoID, RepoInfo.RepoName)

		// Open the repo
		if err := FileTraverser.Repo.OpenRepo(repo_path); err != nil {
			log.Error().Err(err).Msg("unable to open the repo")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		var auth transport.AuthMethod
		switch Payload.Method {
		case "ssh":
			key_path := fmt.Sprintf("%v%v/%v.pem", path, s.Config.GetString("asvatthi.SSH-Keys"), RepoInfo.CloneInfo.KeyName)
			auth, err = fts.SSHBasicAuth(key_path)
			if err != nil {
				log.Error().Err(err).Msg("unable to get auth")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}

		case "oauth":
			auth = fts.OauthBasicAuth(Payload.Auth)

		default:
			log.Error().Err(errors.New("invalid method")).Msg("invalid method")
			c.JSON(http.StatusBadRequest, gin.H{"error": "invalid method"})
			return

		}

		// Fetch
		if err := FileTraverser.Repo.Pull(auth); err != nil {
			if err == fts.NoErrAlreadyUpToDate {
				log.Error().Err(err).Msg(err.Error())
				c.JSON(http.StatusOK, gin.H{"status": err.Error()})
				return
			}
			log.Error().Err(err).Msg("unable to fetch the changes")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		if err := FileTraverser.Sync(c.Request.Context(), RepoInfo); err != nil {
			log.Error().Err(err).Msg("unable to sync the repo")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		data, err := FileTraverser.UpdateDatabaseInfo(c.Request.Context(), RepoInfo)
		if err != nil {
			log.Error().Err(err).Msg("unable to update the repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully synced the repo")
		c.JSON(http.StatusOK, data)
	}
}

func (s *Server) DeleteRepo() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		log.Info().Msg("Payload: " + s.Logger.String(Payload))

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		path := fmt.Sprintf("%v/%v", s.Config.GetString("asvatthi.workdir"), Payload.UserID)
		// repo_name, err := db.GetRepoName(c.Request.Context(), Payload.RepoID)
		// if err != nil {
		// 	log.Error().Err(err).Msg("Unable to get repo_name from arangodb")
		// 	c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		// 	return
		// }

		repo_path := fmt.Sprintf("%v/%v", path, Payload.RepoID)
		if err := os.RemoveAll(repo_path); err != nil {
			log.Error().Err(err).Msg("Repo deletion failed due to Server error")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		branch_ids, err := db.GetBranchIDs(c.Request.Context(), Payload.RepoID)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get branch_ids from arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		if err := db.DeleteCollections(c.Request.Context(), branch_ids); err != nil {
			log.Error().Err(err).Msg("Unable delete arangodb collection")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		if err = db.DeleteRepoInfoDocs(c.Request.Context(), Payload.RepoID); err != nil {
			log.Error().Err(err).Msg("Unable to delete repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully deleted the repo")
		c.JSON(http.StatusOK, gin.H{"status": "successfully deleted the repo"})
	}
}

func (s *Server) GetRepoInfos() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Get payload from request body
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		repos, err := db.GetRepoInfo(c.Request.Context())
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned repo info")
		c.JSON(http.StatusOK, gin.H{"repositories": repos})
	}
}

func (s *Server) GetRepoFiles() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		branch_id, err := db.GetBranchID(c.Request.Context(), Payload.RepoID)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get branch_id")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		files, err := db.GetRepoFiles(c.Request.Context(), branch_id)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo files")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned repo files")
		c.JSON(http.StatusOK, gin.H{"files": files})
	}
}

func (s *Server) GetFileContent() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
			FileID string `json:"file_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		var RepoInfo fts.RepoInfo
		if err := db.GetLastEntry(c.Request.Context(), Payload.RepoID, &RepoInfo); err != nil {
			log.Error().Err(err).Msg("Unable to get repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		filepath, err := db.GetFilePath(c.Request.Context(), Payload.FileID, RepoInfo.BranchID)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get file path")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		abspath := fmt.Sprintf("%v/%v/%v/%v/%v", s.Config.GetString("asvatthi.workdir"), Payload.UserID, RepoInfo.RepoID, RepoInfo.RepoName, filepath)
		content, err := fts.ReadFileContent(abspath)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get file content")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned file content")
		c.String(http.StatusOK, content)
	}
}

func (s *Server) Checkout() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
			Branch string `json:"branch_name" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		// Get arangoDB database from client
		var FileTraverser fts.FileTraverser
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &FileTraverser.DB); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		var RepoInfo fts.RepoInfo
		if err := FileTraverser.DB.GetLastEntry(c.Request.Context(), Payload.RepoID, &RepoInfo); err != nil {
			log.Error().Err(err).Msg("Unable to get repo_name from arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		path := fmt.Sprintf("%v/%v", s.Config.GetString("asvatthi.workdir"), Payload.UserID)

		// Open the repo
		repo_path := fmt.Sprintf("%v/%v/%v", path, RepoInfo.RepoID, RepoInfo.RepoName)
		if err := FileTraverser.Repo.OpenRepo(repo_path); err != nil {
			log.Error().Err(err).Msg("unable to open the repo")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		if err := FileTraverser.Repo.Checkout(Payload.Branch); err != nil {
			log.Error().Err(err).Msg("unable to checkout")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// check if the data is already in arangodb
		branch_id, exists, err := FileTraverser.DB.CheckForBranch(c.Request.Context(), Payload.Branch)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo info from arangodb")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		if !exists {
			branch_id = utils.GetNewUUID(utils.BranchIDPrefix)
			if err = FileTraverser.Traverse(c.Request.Context(), branch_id); err != nil {
				log.Error().Err(err).Msg("unable to traverse the repo")
				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
				return
			}
			RepoInfo.LivedocsBranches = append(RepoInfo.LivedocsBranches, Payload.Branch)
		}

		RepoInfo.Branch = Payload.Branch
		RepoInfo.BranchID = branch_id
		data, err := FileTraverser.UpdateDatabaseInfo(c.Request.Context(), RepoInfo)
		if err != nil {
			log.Error().Err(err).Msg("unable to update the repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		}

		log.Info().Msg("successfully switched branch")
		data["status"] = "successfully switched branch"
		c.JSON(http.StatusOK, data)
	}
}

func (s *Server) GetRepoInfo() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Get payload from request body
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		var RepoInfo fts.RepoInfo
		if err := db.GetLastEntry(c.Request.Context(), Payload.RepoID, &RepoInfo); err != nil {
			log.Error().Err(err).Msg("Unable to get repo info")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned repo info")
		c.JSON(http.StatusOK, RepoInfo)
	}
}

func (s *Server) DeleteUser() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		log.Info().Msg("Payload: " + s.Logger.String(Payload))

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		if err := db.Remove(c.Request.Context()); err != nil {
			log.Error().Err(err).Msg("Unable delete arangodb database")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		path := fmt.Sprintf("%v/%v", s.Config.GetString("asvatthi.workdir"), Payload.UserID)
		if err := os.RemoveAll(path); err != nil {
			log.Error().Err(err).Msg("failed to delete user folder")
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully deleted the user data")
		c.JSON(http.StatusOK, gin.H{"status": "successfully deleted the user data"})
	}
}
