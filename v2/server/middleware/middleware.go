package middleware

import (
	"errors"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	logger "gitlab.com/punitnaik/file-traversal-system-v2/log"
)

// Authorize is a middleware function for session management using JWT
func Authorize(log *logger.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {

		// Get the username and password from environment variables
		FTS_USER := os.Getenv("FTS_USER")
		FTS_PASSWD := os.Getenv("FTS_PASSWD")

		// Check if both environment variables are set
		if FTS_USER == "" || FTS_PASSWD == "" {
			log.Logger.Error().Err(errors.New("username or password is not set")).Msg("username or password is not set")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "username or password is not set"})
			return
		}

		username, password, ok := c.Request.BasicAuth()
		if !ok {
			log.Logger.Error().Err(errors.New("no Basic Auth credentials provided")).Msg("no Basic Auth credentials provided")
			c.JSON(http.StatusUnauthorized, gin.H{"error": "no Basic Auth credentials provided"})
			return
		}

		if FTS_USER != username || FTS_PASSWD != password {
			// Compare it with the request data
			log.Logger.Error().Err(errors.New("username or password does not match")).Msg("username or password does not match")
			c.JSON(http.StatusUnauthorized, gin.H{"error": "username or password does not match"})
			return
		}

		c.Next()
	}
}
