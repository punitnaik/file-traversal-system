package server

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/punitnaik/file-traversal-system-v2/db/arangodb"
	logger "gitlab.com/punitnaik/file-traversal-system-v2/log"
	"gitlab.com/punitnaik/file-traversal-system-v2/server/middleware"
	"gitlab.com/punitnaik/file-traversal-system-v2/utils"
)

// Server serves requests to DB with router
type Server struct {
	ArangoDB *arangodb.ArangoDBClient
	Logger   *logger.Logger
	Config   *utils.Config
}

func (s *Server) defineRoutes(router *gin.Engine) {
	repo := router.Group("/repo")
	repo.Use(middleware.Authorize(s.Logger))
	repo.DELETE("", s.DeleteRepo())
	repo.GET("/infos", s.GetRepoInfos())
	repo.GET("/info", s.GetRepoInfo())
	repo.POST("/checkout", s.Checkout())
	repo.GET("/files", s.GetRepoFiles())
	repo.POST("/clone", s.Clone())
	repo.POST("/fetch", s.Fetch())

	file := router.Group("/file")
	file.Use(middleware.Authorize(s.Logger))
	file.GET("/content", s.GetFileContent())

	user := router.Group("/user", middleware.Authorize(s.Logger))
	user.DELETE("", s.DeleteUser())
}

func (s *Server) setupRouter() *gin.Engine {
	r := gin.Default()
	r.Use(gin.Recovery())

	// Set Gin's default writer to be the multi-writer including Lumberjack and stdout
	gin.DefaultWriter = io.MultiWriter(&s.Logger.GinWriter, os.Stdout)

	// Use the custom logger
	r.Use(gin.LoggerWithConfig(gin.LoggerConfig{
		Output: &s.Logger.GinWriter,
	}))

	s.defineRoutes(r)
	return r
}

func (s *Server) Start() {
	r := s.setupRouter()
	log := s.Logger.Logger
	PORT := ":7349"
	srv := &http.Server{
		Addr:    PORT,
		Handler: r,
	}

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Error().Err(err).Msg("unable to start the server")
		}
	}()

	log.Info().Msg(fmt.Sprintf("Server started on %s", PORT))

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Info().Msg("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Error().Err(err).Msg("Server forced to shutdown")
	}

	log.Info().Msg("Server exiting")
}

func (s *Server) GetRequestPayload(c *gin.Context, requestBody interface{}) error {
	if err := c.ShouldBindJSON(requestBody); err != nil {
		return err
	}

	return nil
}
