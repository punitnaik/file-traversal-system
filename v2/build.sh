#!/bin/bash

IMAGE_NAME="asvatthi-fts"
TAG1="latest"
TAG2="$1" 
DOCKER_USERNAME="oceanholic"  

if [ -z "$TAG2" ]; then
    echo "Usage: $0 <tag>"
    exit 1
fi

docker-compose build

docker tag $IMAGE_NAME $DOCKER_USERNAME/$IMAGE_NAME:$TAG1
docker tag $IMAGE_NAME $DOCKER_USERNAME/$IMAGE_NAME:$TAG2

docker push $DOCKER_USERNAME/$IMAGE_NAME:$TAG1
docker push $DOCKER_USERNAME/$IMAGE_NAME:$TAG2
