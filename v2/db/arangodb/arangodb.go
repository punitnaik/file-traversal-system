package arangodb

import (
	"context"
	"fmt"
	"log"

	driver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
	"gitlab.com/punitnaik/file-traversal-system-v2/utils"
)

type ArangoDBClient struct {
	driver.Client
}

const FilesSuffix = "-Files"
const FolderSuffix = "-Folders"
const ContainsSuffix = "-Contains"
const TreeSuffix = "-Tree"
const InfoCollectionName = "INFO"

type ArangoDB struct {
	driver.Database
}

type DocumentMeta struct {
	driver.DocumentMeta
}

func (a *ArangoDBClient) Init(config *utils.Config) error {
	// Get configaration data
	subConfig := config.Sub("database.arangodb")
	host := subConfig.GetString("host")
	port := subConfig.GetString("port")
	username := subConfig.GetString("login")
	password := subConfig.GetString("pass")
	url := fmt.Sprintf("http://%v:%v", host, port)

	conn, err := a.CreateArangoDBConnection(url)
	if err != nil {
		return err
	}

	if err = a.CreateArangoDBClient(conn, username, password); err != nil {
		return err
	}

	return nil
}

func (a *ArangoDBClient) CreateArangoDBConnection(url string) (driver.Connection, error) {
	// Define the ArangoDB connection parameters
	conn, err := http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{url},
	})
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func (a *ArangoDBClient) CreateArangoDBClient(conn driver.Connection, username, password string) error {
	// Create an ArangoDB client
	var err error
	if a.Client, err = driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication(username, password),
	}); err != nil {
		return err
	}

	return nil
}

func (a *ArangoDBClient) CreateArangoDBDatabase(ctx context.Context, dbName string) (ArangoDB, error) {
	// Create the database
	var err error
	var db ArangoDB
	db.Database, err = a.Client.CreateDatabase(ctx, dbName, nil)
	if err != nil {
		return ArangoDB{}, err
	}

	return db, nil
}

func (a *ArangoDBClient) LoadDatabase(ctx context.Context, dbName string, db *ArangoDB) error {
	// Check if the user already has a database
	exists, err := a.Client.DatabaseExists(ctx, dbName)
	if err != nil {
		// log.Error().Err(err).Msg("unable to check if the database exists or not")
		return err
	}

	// Set up and Load the data to arangoDB
	if exists {
		db.Database, err = a.Client.Database(ctx, dbName)
		if err != nil {
			// log.Error().Err(err).Msg("unable to load arangodb database")
			return err
		}
	} else {
		db.Database, err = a.Client.CreateDatabase(ctx, dbName, nil)
		if err != nil {
			// log.Error().Err(err).Msg("unable to create arangodb database")
			return err
		}
		_, err = db.LoadCollection(ctx, InfoCollectionName)
		if err != nil {
			return err
		}
	}

	return nil
}

func (db *ArangoDB) LoadCollection(ctx context.Context, collection_name string) (driver.Collection, error) {
	// Check if the collection exists
	exists, err := db.Database.CollectionExists(ctx, collection_name)
	if err != nil {
		// log.Error().Err(err).Msg("unable to check if the collection exists or not")
		return nil, err
	}

	var Docs driver.Collection
	if exists {
		Docs, err = db.Collection(ctx, collection_name)
		if err != nil {
			// log.Error().Err(err).Msg("unable to load arangodb collection")
			return nil, err
		}
	} else {
		Docs, err = db.CreateCollection(ctx, collection_name, nil)
		if err != nil {
			// log.Error().Err(err).Msg("unable to create arangodb database")
			return nil, err
		}
	}

	return Docs, nil
}

func (db *ArangoDB) CreateArangoDBGraph(ctx context.Context, branch_id string) (driver.Graph, error) {
	edgeDefinition := driver.EdgeDefinition{
		Collection: branch_id + ContainsSuffix,
		From:       []string{branch_id + FolderSuffix},
		To:         []string{branch_id + FolderSuffix, branch_id + FilesSuffix},
	}
	// Create the graph
	graph, err := db.Database.CreateGraph(ctx, branch_id+TreeSuffix, &driver.CreateGraphOptions{
		EdgeDefinitions: []driver.EdgeDefinition{edgeDefinition},
	})
	if err != nil {
		return nil, err
	}
	return graph, nil
}

func (db *ArangoDB) CreateArangoDBCollection(ctx context.Context, collectionName string, options *driver.CreateCollectionOptions) (driver.Collection, error) {
	//create the collection
	collection, err := db.Database.CreateCollection(ctx, collectionName, options)
	if err != nil {
		return nil, err
	}

	return collection, nil
}

func (db *ArangoDB) CreateArangoDBEdgeCollection(ctx context.Context, collectionName string) (driver.Collection, error) {
	//create the collection
	options := &driver.CreateCollectionOptions{
		Type: driver.CollectionTypeEdge,
	}

	collection, err := db.CreateArangoDBCollection(ctx, collectionName, options)
	if err != nil {
		return nil, err
	}

	return collection, nil
}

func (db *ArangoDB) DeleteArangoDBCollection(ctx context.Context, collectionName string) error {
	col, err := db.Database.Collection(ctx, collectionName)
	if err != nil {
		return err
	}

	// Drop the collection
	if err := col.Remove(ctx); err != nil {
		return err
	}

	return nil
}

func CreateArangoDBDocument(ctx context.Context, collection driver.Collection, data map[string]interface{}) (driver.DocumentMeta, error) {
	//create the Document
	document, err := collection.CreateDocument(ctx, data)
	if err != nil {
		return driver.DocumentMeta{}, err
	}
	return document, nil
}

func DeleteArangoDBDocument(ctx context.Context, collection driver.Collection, key string) (driver.DocumentMeta, error) {
	// Remove the document
	document, err := collection.RemoveDocument(ctx, key)
	if err != nil {
		return driver.DocumentMeta{}, err
	}
	return document, nil
}

func CreateArangoDBEdge(ctx context.Context, collection driver.Collection, fromKey string, toKey string) (driver.DocumentMeta, error) {
	// Create the edge between the documents
	edge := map[string]interface{}{
		"_from": fromKey,
		"_to":   toKey,
	}
	edgeDoc, err := collection.CreateDocument(ctx, edge)
	if err != nil {
		return driver.DocumentMeta{}, err
	}

	return edgeDoc, nil
}

func (db *ArangoDB) QueryArangoDBDatabase(ctx context.Context, query string, bindVars map[string]interface{}) ([]map[string]interface{}, error) {
	// query := `
	// 	FOR doc IN @@collection
	// 	FILTER LIKE(doc.Name, "%.py")
	// 	RETURN doc
	// `
	// bindVars := map[string]interface{}{
	// 	"@collection": "Files",
	// }

	// Execute the query
	cursor, err := db.Database.Query(ctx, query, bindVars)
	if err != nil {
		return nil, err
	}
	defer cursor.Close()

	// Iterate over the results
	var documents []map[string]interface{}
	for {
		var doc map[string]interface{}
		_, err := cursor.ReadDocument(ctx, &doc)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		documents = append(documents, doc)
	}

	return documents, nil
}

func (db *ArangoDB) GetEmptyDocuentMeta() driver.DocumentMeta {
	return driver.DocumentMeta{}
}

func (db *ArangoDB) SetupCollections(ctx context.Context, brach_id string) (driver.Collection, error) {
	Files, err := db.CreateArangoDBCollection(ctx, brach_id+FilesSuffix, nil)
	if err != nil {
		return nil, err
	}

	return Files, nil
}

func (db *ArangoDB) DeleteCollections(ctx context.Context, branch_ids []string) error {
	for _, branch_id := range branch_ids {
		Files, err := db.Database.Collection(ctx, branch_id+FilesSuffix)
		if err != nil {
			return err
		}
		if Files.Remove(ctx); err != nil {
			return err
		}
	}

	return nil
}

func (a *ArangoDBClient) GetDatabase(ctx context.Context, name string) (ArangoDB, error) {
	var err error
	var db ArangoDB
	db.Database, err = a.Database(ctx, name)
	if err != nil {
		return ArangoDB{}, err
	}

	return db, nil
}

func (db *ArangoDB) GetRepoName(ctx context.Context, repo_id string) (string, error) {
	// Query the database to get last modified entry to get the information about the last updated commit
	cursor, err := db.Database.Query(ctx, GetRepoNameFromRepoID, map[string]interface{}{
		"@collection": InfoCollectionName,
		"repo_id":     repo_id,
	})
	if err != nil {
		return "", err
	}
	defer cursor.Close()

	// Read the result
	var repoName string
	_, err = cursor.ReadDocument(ctx, &repoName)
	if err != nil {
		return "", err
	}

	return repoName, nil
}

func (db *ArangoDB) GetLastCommitID(ctx context.Context, repo_id string) (string, error) {
	// Query the database to get last modified entry to get the information about the last updated commit
	cursor, err := db.Database.Query(ctx, GetLastCommitFromRepoID, map[string]interface{}{
		"@collection": InfoCollectionName,
		"repo_id":     repo_id,
	})
	if err != nil {
		return "", err
	}
	defer cursor.Close()

	// Read the result
	var commitID string
	_, err = cursor.ReadDocument(ctx, &commitID)
	if err != nil {
		return "", err
	}

	return commitID, nil
}

func (db *ArangoDB) GetLastEntry(ctx context.Context, repo_id string, doc interface{}) error {
	// Query the database to get last modified entry to get the information about the last updated commit
	cursor, err := db.Database.Query(ctx, GetLastEntry, map[string]interface{}{
		"@collection": InfoCollectionName,
		"repo_id":     repo_id,
	})
	if err != nil {
		return err
	}
	defer cursor.Close()

	// Read the result
	_, err = cursor.ReadDocument(ctx, &doc)
	if err != nil {
		return err
	}

	return nil
}

func (db *ArangoDB) GetBranchIDFromBranch(ctx context.Context, repo_id string, doc interface{}) error {
	// Query the database to get last modified entry to get the information about the last updated commit
	cursor, err := db.Database.Query(ctx, GetBranchIDFromBranch, map[string]interface{}{
		"@collection": InfoCollectionName,
		"repo_id":     repo_id,
	})
	if err != nil {
		return err
	}
	defer cursor.Close()

	// Read the result
	_, err = cursor.ReadDocument(ctx, &doc)
	if err != nil {
		return err
	}

	return nil
}

func (db *ArangoDB) GetRepoInfo(ctx context.Context) ([]map[string]interface{}, error) {
	cursor, err := db.Database.Query(ctx, GetRepoInfo, map[string]interface{}{
		"@collection": InfoCollectionName,
	})
	if err != nil {
		return nil, err
	}
	defer cursor.Close()

	var documents []map[string]interface{}
	for {
		var doc interface{}
		_, err := cursor.ReadDocument(ctx, &doc)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			return nil, err
		}
		documents = append(documents, doc.(map[string]interface{}))
	}

	return documents, nil
}
func (db *ArangoDB) GetRepoFiles(ctx context.Context, brach_id string) ([]map[string]interface{}, error) {
	cursor, err := db.Database.Query(ctx, GetRepoFiles, map[string]interface{}{
		"@files_collection": brach_id + FilesSuffix,
	})
	if err != nil {
		return nil, err
	}

	// Read the result
	var documents []map[string]interface{}
	for {
		var doc interface{}
		_, err := cursor.ReadDocument(ctx, &doc)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			return nil, err
		}
		documents = append(documents, doc.(map[string]interface{}))
	}

	return documents, nil
}

func (db *ArangoDB) GetBranchID(ctx context.Context, repo_id string) (string, error) {
	// Get the branch id
	cursor, err := db.Database.Query(ctx, GetBranchID, map[string]interface{}{
		"@collection": InfoCollectionName,
		"repo_id":     repo_id,
	})
	if err != nil {
		return "", err
	}
	defer cursor.Close()

	var brach_id string
	_, err = cursor.ReadDocument(ctx, &brach_id)
	if err != nil {
		return "", err
	}

	return brach_id, nil
}

func (db *ArangoDB) GetFilePath(ctx context.Context, file_id, brach_id string) (string, error) {
	// Get the branch id
	cursor, err := db.Database.Query(ctx, GetFilePath, map[string]interface{}{
		"@collection": brach_id + FilesSuffix,
		"file_id":     file_id,
	})
	if err != nil {
		return "", err
	}
	defer cursor.Close()

	var filepath string
	_, err = cursor.ReadDocument(ctx, &filepath)
	if err != nil {
		return "", err
	}

	return filepath, nil
}

func (db *ArangoDB) DeleteRepoInfoDocs(ctx context.Context, repo_id string) error {
	// Get the branch id
	cursor, err := db.Database.Query(ctx, DeleteRepoInfoDocs, map[string]interface{}{
		"@collection": InfoCollectionName,
		"repo_id":     repo_id,
	})
	if err != nil {
		return err
	}
	defer cursor.Close()

	return nil
}

func (db *ArangoDB) CheckForBranch(ctx context.Context, branch string) (string, bool, error) {
	// Get the branch id
	cursor, err := db.Database.Query(ctx, CheckForBranch, map[string]interface{}{
		"@collection": InfoCollectionName,
		"branch":      branch,
	})
	if err != nil {
		return "", false, err
	}
	defer cursor.Close()

	exists := false
	var branch_id string
	if cursor.HasMore() {
		exists = true
		_, err = cursor.ReadDocument(ctx, &branch_id)
		if err != nil {
			return "", false, err
		}
	}

	return branch_id, exists, nil
}

func (db *ArangoDB) GetBranchIDs(ctx context.Context, repo_id string) ([]string, error) {
	cursor, err := db.Database.Query(ctx, GetBranchIDs, map[string]interface{}{
		"@collection": InfoCollectionName,
		"repo_id":     repo_id,
	})
	if err != nil {
		return nil, err
	}

	// Read the result
	var documents []string
	for {
		var doc string
		_, err := cursor.ReadDocument(ctx, &doc)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			return nil, err
		}
		documents = append(documents, doc)
	}

	return documents, nil
}
