package arangodb

const GetDocByExt = `
	FOR doc IN @@collection
	FILTER doc.extension == @extension
	RETURN doc
`

const Query = `
	FOR doc IN @@collection
	COLLECT fileType = doc.extension WITH COUNT INTO count
	RETURN { fileType, count }
`
const GetDocByFilepath = `
	FOR doc IN @@collection
	FILTER doc.Path == @fileName
	RETURN doc
`

const GetLastDocument = `
	FOR doc IN @@collection 
	SORT doc.last_synced DESC 
	LIMIT 1 
	RETURN doc
`
const GetDocFromFileInfo = `
	FOR doc IN @@collection 
	FILTER doc.file_path == @file_path 
	FILTER doc.name == @name 
	FILTER doc.parent_path == @parent_path 
	LIMIT 1
	RETURN doc
`
const GetContainsDocFrom_to = `
	FOR doc IN @@collection 
	FILTER doc._to == @ID 
	LIMIT 1
	RETURN doc
`

const GetRepoNameFromRepoID = `
FOR doc IN @@collection
	FILTER doc.repo_id == @repo_id
	LIMIT 1
	RETURN doc.repo_name
`
const GetLastCommitFromRepoID = `
	FOR doc IN @@collection
	FILTER doc.repo_id == @repo_id
	SORT doc.last_synced DESC
	LIMIT 1
	RETURN doc.CommitHash
`
const GetLastEntry = `
FOR doc IN @@collection
	FILTER doc.repo_id == @repo_id
	SORT doc.last_synced DESC
	LIMIT 1
	RETURN doc
`
const GetRepoInfo = `
LET RepoIDs = (
	FOR doc IN @@collection
		COLLECT RepoID = doc.repo_id 
		RETURN RepoID
	)

FOR RepoID IN RepoIDs
	LET UniqueIDs = ( 
		FOR doc IN @@collection
			FILTER doc.repo_id == RepoID
			RETURN doc
	)
	LET date_added = (
		FOR doc IN UniqueIDs
			SORT doc.last_synced ASC
			LIMIT 1
			RETURN doc.last_synced
	)
	LET result = (
		FOR doc IN UniqueIDs
			SORT doc.last_synced DESC
			LIMIT 1
			RETURN doc
	)
	RETURN {"repo_id" : result[0].repo_id,
	        "repo_name" : result[0].repo_name,
            "branch" : result[0].branch_names,
            "external_repo_url" : result[0].repo_url,
	        "date_added" : date_added[0],
	        "last_synced" : result[0].last_synced,
            "last_synced_commit_id" : result[0].commit_id,
            "current_branch" : result[0].branch,
	        }
`
const GetBranchID = `
FOR doc IN @@collection
	FILTER doc.repo_id == @repo_id
	SORT doc.last_synced DESC
	LIMIT 1
	RETURN doc.branch_id
`
const GetRepoFiles = `
FOR doc IN @@files_collection
	RETURN {
		"file_id" : doc._id,
		"file_name": doc.name,
		"file_path": doc.file_path,
		"current_commit_time": doc.current_commit_time != null ? doc.current_commit_time : null,
		"current_commit": doc.current_commit != null ? doc.current_commit : null
	}
`
const GetFilePath = `
	FOR doc IN @@collection
	FILTER doc._id == @file_id
	LIMIT 1
	RETURN doc.file_path
`

const DeleteRepoInfoDocs = `
FOR doc in @@collection
	FILTER doc.repo_id == @repo_id
	REMOVE doc IN @@collection
`

const CheckForBranch = `
FOR doc in @@collection
	FILTER doc.branch == @branch
	LIMIT 1
	RETURN doc.branch_id
`

const GetBranchIDs = `
FOR doc IN @@collection
	FILTER doc.repo_id ==@repo_id 
	COLLECT BranchID = doc.branch_id
	RETURN BranchID

`
const GetBranchIDFromBranch = `
LET DOC = (
	FOR doc IN @@collection
		FILTER doc.repo_id == @repo_id
		SORT doc.last_synced DESC
		LIMIT 1
		RETURN doc
)

FOR Branch in DOC
	FOR doc IN @@collection
		FILTER doc.branch == Branch.branch
		LIMIT 1
		RETURN 	{ [Branch.branch]: doc.branch_id }

`
