package utils

import (
	"errors"
	"math/rand"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
	"time"

	"github.com/google/uuid"
)

type ExitCode struct {
	Code int
	Err  error
	Msg  string
}

const RepoIDPrefix = "repo-"
const BranchIDPrefix = "branch-"
const UserIDPrefix = "user-"

func GetUTCTimestamp() string {
	// Get the current time in UTC
	return time.Now().UTC().Format("2006-01-02T15:04:05.999999999Z07:00")
}

func GenerateRandomString(size int, strType string) string {
	source := rand.NewSource(time.Now().UnixNano())
	rng := rand.New(source)

	const (
		numericChars      = "0123456789"
		alphabetChars     = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		alphanumericChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	)
	var chars string

	switch strType {
	case "numeric":
		chars = numericChars
	case "alphabet":
		chars = alphabetChars
	case "alphanumeric":
		chars = alphanumericChars
	default:
		return ""
	}

	result := make([]byte, size)
	for i := 0; i < size; i++ {
		result[i] = chars[rng.Intn(len(chars))]
	}
	return string(result)
}

func CreateFolder(folderPath string) error {
	// Check if the folder already exists
	if _, err := os.Stat(folderPath); os.IsNotExist(err) {
		// Create the folder
		err := os.Mkdir(folderPath, os.ModePerm)
		if err != nil {
			return err
		}
	} else {
		return errors.New("Folder " + folderPath + " already exists")
	}

	return nil
}

func GetNewUUID(prefix string) string {
	return prefix + uuid.New().String()
}

// extractRepoName extracts the repository name from the Git URL
func ExtractRepoName(repo_url string) string {
	// Remove any trailing '.git' suffix
	repo_url = strings.TrimSuffix(repo_url, ".git")

	// Split the URL by '/' or ':'
	parts := strings.Split(repo_url, "/")
	if len(parts) == 1 {
		parts = strings.Split(parts[0], ":")
	}

	// The last part of the URL is the repository name
	repo_name := parts[len(parts)-1]

	return repo_name
}

func GetFolderPath(filePath string) string {
	// Using filepath.Dir to get the directory path
	dirPath := filepath.Dir(filePath)
	if dirPath == "." {
		return ""
	}
	return dirPath
}

func GetFileName(filePath string) string {
	if filePath == "" {
		return filePath
	}
	return filePath[strings.LastIndex(filePath, "/")+1:]
}

func Convert(input interface{}) map[string]interface{} {
	values := make(map[string]interface{})
	objVal := reflect.ValueOf(input)
	objType := objVal.Type()

	for i := 0; i < objVal.NumField(); i++ {
		field := objType.Field(i)
		fieldName := field.Name
		fieldValue := objVal.Field(i).Interface()

		// Check if the field has a JSON tag
		tag := field.Tag.Get("json")
		if tag != "" && tag != "-" {
			fieldName = tag
		}

		values[fieldName] = fieldValue
	}

	return values
}

// func GetHostingSite(repoURL string) string {
// 	githubRegex := regexp.MustCompile(`github\.com[/:]`)
// 	gitlabRegex := regexp.MustCompile(`gitlab\.com[/:]`)
// 	bitbucketRegex := regexp.MustCompile(`bitbucket\.org[/:]`)

// 	if githubRegex.MatchString(repoURL) {
// 		return "github"
// 	} else if gitlabRegex.MatchString(repoURL) {
// 		return "gitlab"
// 	} else if bitbucketRegex.MatchString(repoURL) {
// 		return "bitbucket"
// 	}

// 	return "unknown"
// }

func GetHostingSite(url string) string {
	// Define regex patterns for common platforms
	gitlabPattern := regexp.MustCompile(`(?:https?://)?(?:[^./]+\.)*gitlab\.[a-z]+`)
	githubPattern := regexp.MustCompile(`(?:https?://)?(?:[^./]+\.)*github\.[a-z]+`)
	bitbucketPattern := regexp.MustCompile(`(?:https?://)?(?:[^./]+\.)*bitbucket\.[a-z]+`)

	// Check if the URL matches any of the patterns
	switch {
	case gitlabPattern.MatchString(url):
		return "gitlab"
	case githubPattern.MatchString(url):
		return "github"
	case bitbucketPattern.MatchString(url):
		return "bitbucket"
	default:
		return "unknown"
	}
}
