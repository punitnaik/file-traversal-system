package oauth

import (
	"errors"

	"github.com/dgrijalva/jwt-go"
)

type JWT struct {
	Key string `json:"key"`
}

func GenerateJWT(key string, claims map[string]interface{}) (string, error) {
	// Create the token object with claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims(claims))

	tokenString, err := token.SignedString([]byte(key))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func ValidateToken(key, tokenString string) (jwt.MapClaims, error) {
	// Parse the token string into a token object
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(key), nil
	})
	if err != nil {
		return nil, err
	}

	// Check if the token is valid
	if !token.Valid {
		return nil, errors.New("invalid token")
	}

	// Extract claims from the token
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("error parsing claims")
	}

	return claims, nil
}
