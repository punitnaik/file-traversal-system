package server

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"reflect"
	"syscall"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	oauth "gitlab.com/punitnaik/file-traversal-system-v3/OAuth"
	"gitlab.com/punitnaik/file-traversal-system-v3/db/arangodb"
	logger "gitlab.com/punitnaik/file-traversal-system-v3/log"
	"gitlab.com/punitnaik/file-traversal-system-v3/server/middleware"
	"gitlab.com/punitnaik/file-traversal-system-v3/utils"
)

// Server serves requests to DB with router
type Server struct {
	ArangoDB *arangodb.ArangoDBClient
	Logger   *logger.Logger
	Config   *utils.Config
	JWT      *oauth.JWT
}

type JSON map[string]interface{}

func (s *Server) defineRoutes(router *gin.Engine) {
	repo := router.Group("/repo")
	repo.Use(middleware.Authorize(s.Logger, s.Config))
	repo.DELETE("", s.DeleteRepo())
	repo.GET("/infos", s.GetRepoInfos())
	repo.GET("/info", s.GetRepoInfo())
	repo.GET("/checkout", s.Checkout())
	repo.GET("/files", s.GetRepoFiles())
	repo.GET("/clone", s.Clone())
	repo.GET("/fetch", s.Fetch())

	file := router.Group("/file")
	file.Use(middleware.Authorize(s.Logger, s.Config))
	file.GET("/content", s.GetFileContent())

	user := router.Group("/user", middleware.Authorize(s.Logger, s.Config))
	user.DELETE("", s.DeleteUser())
}

func (s *Server) setupRouter() *gin.Engine {
	r := gin.Default()
	r.Use(gin.Recovery())

	// Set Gin's default writer to be the multi-writer including Lumberjack and stdout
	gin.DefaultWriter = io.MultiWriter(&s.Logger.GinWriter, os.Stdout)

	// Use the custom logger
	r.Use(gin.LoggerWithConfig(gin.LoggerConfig{
		Output: &s.Logger.GinWriter,
	}))

	s.defineRoutes(r)
	return r
}

func (s *Server) Start() {
	r := s.setupRouter()
	log := s.Logger.Logger
	PORT := ":7349"
	srv := &http.Server{
		Addr:    PORT,
		Handler: r,
	}

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Error().Err(err).Msg("unable to start the server")
		}
	}()

	log.Info().Msg(fmt.Sprintf("Server started on %s", PORT))

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Info().Msg("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Error().Err(err).Msg("Server forced to shutdown")
	}

	log.Info().Msg("Server exiting")
}

func (s *Server) GetRequestPayload(c *gin.Context, requestBody interface{}) error {
	payload, ok := c.Get("payload")
	if !ok {
		return errors.New("payload not found")
	}
	payload_map := payload.(jwt.MapClaims)

	// Get the type of struct
	structType := reflect.TypeOf(requestBody).Elem()

	// Iterate over the struct fields
	for i := 0; i < structType.NumField(); i++ {
		field := structType.Field(i)
		// Check if the field is required
		if binding := field.Tag.Get("binding"); binding == "required" {
			// Check if the field exists in the map
			if value, ok := payload_map[field.Tag.Get("json")]; !ok || value == nil {
				err := errors.New("required field missing: " + field.Tag.Get("json"))
				return err
			}
		}
	}

	// Iterate over the map
	for key, value := range payload_map {
		// Find the corresponding field in the struct
		for i := 0; i < structType.NumField(); i++ {
			field := structType.Field(i)
			if field.Tag.Get("json") == key {
				// Set the value of the field in the struct
				fieldValue := reflect.ValueOf(requestBody).Elem().FieldByName(field.Name)
				if fieldValue.IsValid() && fieldValue.CanSet() {
					if field.Type.Kind() == reflect.Slice && reflect.TypeOf(value).Kind() == reflect.Slice {
						// Convert interface{} slice to the appropriate type
						slice := reflect.ValueOf(value)
						newSlice := reflect.MakeSlice(field.Type, slice.Len(), slice.Len())
						for j := 0; j < slice.Len(); j++ {
							newSlice.Index(j).Set(reflect.ValueOf(slice.Index(j).Interface()).Convert(field.Type.Elem()))
						}
						fieldValue.Set(newSlice)
					} else {
						fieldValue.Set(reflect.ValueOf(value))
					}
				}
				break
			}
		}
	}
	return nil
}

func (s *Server) JSON(c *gin.Context, code int, requestBody interface{}) {
	log := s.Logger.Logger

	var claims JSON
	if _, ok := requestBody.(JSON); ok {
		claims = requestBody.(JSON)
	} else if reflect.TypeOf(requestBody).Kind() == reflect.Map && reflect.TypeOf(requestBody).Elem().Kind() == reflect.Interface {
		claims = JSON(requestBody.(map[string]interface{}))
	} else {
		claims = JSON(utils.Convert(requestBody))
	}

	token, err := oauth.GenerateJWT(s.Config.GetString("oauth.jwt.key"), claims)
	if err != nil {
		log.Error().Err(err).Msg("failed to generate token")
		return
	}

	c.JSON(code, token)
}
