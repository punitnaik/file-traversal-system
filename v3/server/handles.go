package server

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/punitnaik/file-traversal-system-v3/db/arangodb"
	"gitlab.com/punitnaik/file-traversal-system-v3/fts"
	"gitlab.com/punitnaik/file-traversal-system-v3/utils"
)

func (s *Server) Clone() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID  string `json:"user_id" binding:"required"`
			RepoURL string `json:"repo_url" binding:"required"`
			Method  string `json:"method"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			s.JSON(c, http.StatusBadRequest, JSON{"error": err.Error()})
			return
		}
		log.Info().Msg("Payload: " + s.Logger.String(Payload))

		FileTraverser := fts.NewFileTraverser(s.Config, nil, nil)
		RepoInfo, err := FileTraverser.Clone(Payload.RepoURL, Payload.UserID, Payload.Method, s.Config)
		if err != nil {
			log.Error().Err(err).Msg("unable to clone the repo")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		// Set up and Load the data to arangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, FileTraverser.DB); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		branch_id := utils.GetNewUUID(utils.BranchIDPrefix)
		if err := FileTraverser.Traverse(c.Request.Context(), branch_id, Payload.RepoURL, RepoInfo.Branch); err != nil {
			log.Error().Err(err).Msg("unable to traverse the repo")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		RepoInfo.BranchID = branch_id
		data, err := FileTraverser.UpdateDatabaseInfo(c.Request.Context(), RepoInfo)
		if err != nil {
			log.Error().Err(err).Msg("unable to update the repo info")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully cloned the repo")
		s.JSON(c, http.StatusOK, data)
	}
}

func (s *Server) Fetch() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			s.JSON(c, http.StatusBadRequest, JSON{"error": err.Error()})
			return
		}
		log.Info().Msg("Payload: " + s.Logger.String(Payload))

		// Get arangoDB database from client
		FileTraverser := fts.NewFileTraverser(s.Config, nil, nil)
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, FileTraverser.DB); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		var RepoInfo fts.RepoInfo
		err := FileTraverser.DB.GetLastEntry(c.Request.Context(), Payload.RepoID, &RepoInfo)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo_name from arangodb")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		if err := FileTraverser.Sync(s.Config, c.Request.Context(), &RepoInfo, Payload.UserID); err != nil {
			log.Error().Err(err).Msg("unable to sync the repo")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		data, err := FileTraverser.UpdateDatabaseInfo(c.Request.Context(), RepoInfo)
		if err != nil {
			log.Error().Err(err).Msg("unable to update the repo info")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully synced the repo")
		s.JSON(c, http.StatusOK, data)
	}
}

func (s *Server) DeleteRepo() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			s.JSON(c, http.StatusBadRequest, JSON{"error": err.Error()})
			return
		}
		log.Info().Msg("Payload: " + s.Logger.String(Payload))

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		path := fmt.Sprintf("%v/%v", s.Config.GetString("asvatthi.workdir"), Payload.UserID)
		// repo_name, err := db.GetRepoName(c.Request.Context(), Payload.RepoID)
		// if err != nil {
		// 	log.Error().Err(err).Msg("Unable to get repo_name from arangodb")
		// 	s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
		// 	return
		// }

		repo_path := fmt.Sprintf("%v/%v", path, Payload.RepoID)
		if err := os.RemoveAll(repo_path); err != nil {
			log.Error().Err(err).Msg("Repo deletion failed due to Server error")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		branch_ids, err := db.GetBranchIDs(c.Request.Context(), Payload.RepoID)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get branch_ids from arangodb")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}
		if err := db.DeleteCollections(c.Request.Context(), branch_ids); err != nil {
			log.Error().Err(err).Msg("Unable delete arangodb collection")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		if err = db.DeleteRepoInfoDocs(c.Request.Context(), Payload.RepoID); err != nil {
			log.Error().Err(err).Msg("Unable to delete repo info")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully deleted the repo")
		s.JSON(c, http.StatusOK, JSON{"status": "successfully deleted the repo"})
	}
}

func (s *Server) GetRepoInfos() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Get payload from request body
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			s.JSON(c, http.StatusBadRequest, JSON{"error": err.Error()})
			return
		}

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		repos, err := db.GetRepoInfo(c.Request.Context())
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo info")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned repo info")
		s.JSON(c, http.StatusOK, JSON{"repositories": repos})
	}
}

func (s *Server) GetRepoFiles() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			s.JSON(c, http.StatusBadRequest, JSON{"error": err.Error()})
			return
		}

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		branch_id, err := db.GetBranchID(c.Request.Context(), Payload.RepoID)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get branch_id")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		files, err := db.GetRepoFiles(c.Request.Context(), branch_id)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo files")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned repo files")
		s.JSON(c, http.StatusOK, JSON{"files": files})
	}
}

func (s *Server) GetFileContent() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
			FileID string `json:"file_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			s.JSON(c, http.StatusBadRequest, JSON{"error": err.Error()})
			return
		}

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		var RepoInfo fts.RepoInfo
		if err := db.GetLastEntry(c.Request.Context(), Payload.RepoID, &RepoInfo); err != nil {
			log.Error().Err(err).Msg("Unable to get repo info")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		filepath, err := db.GetFilePath(c.Request.Context(), Payload.FileID, RepoInfo.BranchID)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get file path")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		// content, err := fts.ReadFileContent(abspath)
		// if err != nil {
		// 	log.Error().Err(err).Msg("Unable to get file content")
		// 	s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
		// 	return
		// }

		abspath := fmt.Sprintf("%v/%v/%v/%v/%v", s.Config.GetString("asvatthi.workdir"), Payload.UserID, RepoInfo.RepoID, RepoInfo.RepoName, filepath)
		log.Info().Msg("successfully returned file content")
		s.JSON(c, http.StatusOK, JSON{"file_path": abspath})
	}
}

func (s *Server) Checkout() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
			Branch string `json:"branch_name" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			s.JSON(c, http.StatusBadRequest, JSON{"error": err.Error()})
			return
		}

		// Get arangoDB database from client
		FileTraverser := fts.NewFileTraverser(s.Config, nil, nil)
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, FileTraverser.DB); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		var RepoInfo fts.RepoInfo
		if err := FileTraverser.DB.GetLastEntry(c.Request.Context(), Payload.RepoID, &RepoInfo); err != nil {
			log.Error().Err(err).Msg("Unable to get repo_name from arangodb")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}
		path := fmt.Sprintf("%v/%v", s.Config.GetString("asvatthi.workdir"), Payload.UserID)

		// Open the repo
		repo_path := fmt.Sprintf("%v/%v/%v", path, RepoInfo.RepoID, RepoInfo.RepoName)
		if err := FileTraverser.Repo.OpenRepo(repo_path); err != nil {
			log.Error().Err(err).Msg("unable to open the repo")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		if err := FileTraverser.Repo.Checkout(Payload.Branch); err != nil {
			log.Error().Err(err).Msg("unable to checkout")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		// check if the data is already in arangodb
		branch_id, exists, err := FileTraverser.DB.CheckForBranch(c.Request.Context(), Payload.Branch, RepoInfo.RepoID)
		if err != nil {
			log.Error().Err(err).Msg("Unable to get repo info from arangodb")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		if !exists {
			branch_id = utils.GetNewUUID(utils.BranchIDPrefix)
			if err = FileTraverser.Traverse(c.Request.Context(), branch_id, RepoInfo.RepoURL, Payload.Branch); err != nil {
				log.Error().Err(err).Msg("unable to traverse the repo")
				s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
				return
			}
			RepoInfo.LivedocsBranches = append(RepoInfo.LivedocsBranches, Payload.Branch)
		}

		RepoInfo.Branch = Payload.Branch
		RepoInfo.BranchID = branch_id
		data, err := FileTraverser.UpdateDatabaseInfo(c.Request.Context(), RepoInfo)
		if err != nil {
			log.Error().Err(err).Msg("unable to update the repo info")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
		}

		log.Info().Msg("successfully switched branch")
		s.JSON(c, http.StatusOK, data)
	}
}

func (s *Server) GetRepoInfo() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Get payload from request body
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
			RepoID string `json:"repo_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			s.JSON(c, http.StatusBadRequest, JSON{"error": err.Error()})
			return
		}

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		var RepoInfo fts.RepoInfo
		if err := db.GetLastEntry(c.Request.Context(), Payload.RepoID, &RepoInfo); err != nil {
			log.Error().Err(err).Msg("Unable to get repo info")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully returned repo info")
		s.JSON(c, http.StatusOK, RepoInfo)
	}
}

func (s *Server) DeleteUser() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := s.Logger.Logger

		// Define the payload
		var Payload struct {
			UserID string `json:"user_id" binding:"required"`
		}

		// Bind JSON input to the anonymous struct
		if err := s.GetRequestPayload(c, &Payload); err != nil {
			log.Error().Err(err).Msg("failed to bind json")
			s.JSON(c, http.StatusBadRequest, JSON{"error": err.Error()})
			return
		}
		log.Info().Msg("Payload: " + s.Logger.String(Payload))

		// Get arangoDB database from client
		var db arangodb.ArangoDB
		if err := s.ArangoDB.LoadDatabase(c.Request.Context(), Payload.UserID, &db); err != nil {
			log.Error().Err(err).Msg("unable to load arangodb database")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		if err := db.Remove(c.Request.Context()); err != nil {
			log.Error().Err(err).Msg("Unable delete arangodb database")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		path := fmt.Sprintf("%v/%v", s.Config.GetString("asvatthi.workdir"), Payload.UserID)
		if err := os.RemoveAll(path); err != nil {
			log.Error().Err(err).Msg("failed to delete user folder")
			s.JSON(c, http.StatusInternalServerError, JSON{"error": err.Error()})
			return
		}

		log.Info().Msg("successfully deleted the user data")
		s.JSON(c, http.StatusOK, JSON{"status": "successfully deleted the user data"})
	}
}
