package middleware

import (
	"errors"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	oauth "gitlab.com/punitnaik/file-traversal-system-v3/OAuth"
	logger "gitlab.com/punitnaik/file-traversal-system-v3/log"
	"gitlab.com/punitnaik/file-traversal-system-v3/utils"
)

// Authorize is a middleware function for session management using JWT
func Authorize(log *logger.Logger, config *utils.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get the Authorization header from the request
		authorizationHeader := c.GetHeader("Authorization")

		var token string
		// Check if the Authorization header is not empty and starts with "Bearer "
		if authorizationHeader != "" && strings.HasPrefix(authorizationHeader, "Bearer ") {
			// Extract the token part after "Bearer "
			token = strings.TrimPrefix(authorizationHeader, "Bearer ")
		} else {
			log.Logger.Error().Err(errors.New("token not found")).Msg("token not found")
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "token not found"})
			return
		}

		// Validate token
		tokenData, err := oauth.ValidateToken(config.GetString("oauth.jwt.key"), token)
		if err != nil {
			// Token is not valid, reauthenticate
			log.Logger.Error().Err(err).Msg("failed to validate token")
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		// Set payload in the Gin context
		c.Set("payload", tokenData)
		c.Next()
	}
}
