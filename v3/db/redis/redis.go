package redis

import (
	"context"
	"encoding/json"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/punitnaik/file-traversal-system-v3/utils"
)

const Nil = redis.Nil

type RedisClient struct {
	*redis.Client
}

type Message struct {
	Src  string `json:"src"`
	Dest string `json:"dest"`
}

func NewRedisClient(config *utils.Config) *RedisClient {
	// Initialize Redis client
	subConfig := config.Sub("database.redis")
	return &RedisClient{
		Client: redis.NewClient(&redis.Options{
			Addr:     subConfig.GetString("host") + ":" + subConfig.GetString("port"),
			Password: subConfig.GetString("Password"),
			DB:       0,
		}),
	}

}

func (r *RedisClient) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) error {
	return r.Client.Set(ctx, key, value, expiration).Err()
}

func (r *RedisClient) GetAccessToken(ctx context.Context, key string) (string, error) {
	token, err := r.Client.Get(ctx, key).Result()
	if err == redis.Nil {
		token = ""
	} else if err != nil {
		return "", err
	}

	return token, nil
}

func (r *RedisClient) Publish(ctx context.Context, channel string, message Message) error {
	// Convert the message to JSON
	messageBytes, err := json.Marshal(message)
	if err != nil {
		return err
	}

	// Publish the JSON message to the specified channel
	result := r.Client.Publish(ctx, channel, messageBytes)

	// Check for errors during publishing
	if result.Err() != nil {
		return result.Err()
	}
	return nil
}
