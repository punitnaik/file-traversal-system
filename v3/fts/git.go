package fts

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/crypto/ssh"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/config"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
	gitssh "gopkg.in/src-d/go-git.v4/plumbing/transport/ssh"
)

var NoErrAlreadyUpToDate = git.NoErrAlreadyUpToDate

type GitRepo struct {
	*git.Repository
}

func NewGitRepo() *GitRepo {
	return &GitRepo{
		Repository: nil,
	}
}

func (r *GitRepo) Clone(path, repo_url string, auth transport.AuthMethod) error {
	var err error

	// Clone the repository
	cloneOptions := CloneOptions(repo_url, auth)
	if r.Repository, err = git.PlainClone(path, false, cloneOptions); err != nil {
		return err
	}

	if r.Pull(auth); err != nil {
		return err
	}
	return nil
}

func (r *GitRepo) Pull(auth transport.AuthMethod) error {
	// Fetch the latest changes from the remote repository
	fetchOptions := FetchOptions(auth)
	if err := r.Repository.Fetch(fetchOptions); err != git.NoErrAlreadyUpToDate || err != nil {
		return err
	}

	return nil
}

func (r *GitRepo) OpenRepo(repoPath string) error {
	// Open the repository
	var err error
	r.Repository, err = git.PlainOpen(repoPath)
	if err != nil {
		return err
	}

	return nil
}

func (r *GitRepo) GetCommitHash() (plumbing.Hash, error) {
	headref, err := r.Repository.Head()
	if err != nil {
		return plumbing.Hash{}, err
	}

	return headref.Hash(), nil
}

func (r *GitRepo) GetCommitID() (string, error) {
	headref, err := r.Repository.Head()
	if err != nil {
		return "", err
	}

	return headref.Hash().String(), nil
}

func GetCommitTime(repo *git.Repository) (string, error) {
	// Retrieve the commit object at the HEAD reference.
	headRef, err := repo.Head()
	if err != nil {
		return "", err
	}

	commitObj, err := repo.CommitObject(headRef.Hash())
	if err != nil {
		return "", err
	}

	return commitObj.Committer.When.UTC().Format("2006-01-02T15:04:05.999999999Z07:00"), nil
}

func (r *GitRepo) Branch() (string, error) {
	headRef, err := r.Repository.Head()
	if err != nil {
		return "", err
	}

	return headRef.Name().Short(), nil
}

func (r *GitRepo) Branches() ([]string, error) {
	branches, err := r.Repository.References()
	if err != nil {
		return nil, err
	}

	// Iterate over the branches and print their names
	var branchNames []string
	err = branches.ForEach(func(ref *plumbing.Reference) error {
		// Reference name is in the format "refs/heads/branch_name"
		if ref.Name().IsRemote() && ref.Name() != "refs/remotes/origin/HEAD" {
			// branchName := ref.Name().Short()
			branchName := strings.TrimPrefix(ref.Name().Short(), "origin/")
			branchNames = append(branchNames, branchName)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	return branchNames, nil
}

func GetHash(commitID string) plumbing.Hash {
	return plumbing.NewHash(commitID)
}

func (r *GitRepo) GetCommitObject() (*object.Commit, error) {
	commitHash, err := r.GetCommitHash()
	if err != nil {
		return nil, err
	}
	commit, err := r.Repository.CommitObject(commitHash)
	if err != nil {
		return nil, err
	}

	return commit, nil
}

func (r *GitRepo) GetCommitObjectFromID(commitHash plumbing.Hash) (*object.Commit, error) {
	commit, err := r.Repository.CommitObject(commitHash)
	if err != nil {
		return nil, err
	}

	return commit, nil
}

func OauthBasicAuth(access_token string) transport.AuthMethod {
	if access_token != "" {
		return &http.BasicAuth{
			Username: "oauth2",
			Password: access_token,
		}
	}

	return nil
}

func SSHBasicAuth(keyPath string) (transport.AuthMethod, error) {
	auth, err := GetAuthDetails(keyPath)
	if err != nil {
		return nil, err
	}

	return auth, nil
}

func SSHBasicAuthList(folderPath string) ([]transport.AuthMethod, error) {
	// Get list of files in the SSH keys folder
	keyFiles, err := os.ReadDir(folderPath)
	if err != nil {
		return nil, err
	}

	var auth []transport.AuthMethod
	// Iterate over each key file
	for _, keyFile := range keyFiles {
		// Construct full path to the key file
		keyPath := filepath.Join(folderPath, keyFile.Name())

		key, err := GetAuthDetails(keyPath)
		if err != nil {
			continue
		}

		auth = append(auth, key)
	}

	if auth == nil {
		return nil, errors.New("no ssh keys")
	}

	return auth, nil
}

func CloneOptions(repoUrl string, auth transport.AuthMethod) *git.CloneOptions {
	return &git.CloneOptions{
		URL:          repoUrl,
		SingleBranch: false,
		Auth:         auth,
	}
}

func FetchOptions(auth transport.AuthMethod) *git.FetchOptions {
	return &git.FetchOptions{
		RefSpecs: []config.RefSpec{"refs/*:refs/*"},
		Auth:     auth,
	}
}

func GetAuthDetails(keyPath string) (*gitssh.PublicKeys, error) {
	// Set up SSH authentication
	auth, err := gitssh.NewPublicKeysFromFile("git", keyPath, "")
	if err != nil {
		return nil, err
	}
	auth.HostKeyCallback = ssh.InsecureIgnoreHostKey()

	return auth, nil
}

func (repo *GitRepo) GetGitRelatedInfo(filePath string) (*object.Commit, error) {
	// Get the file history
	history, err := repo.Repository.Log(&git.LogOptions{
		FileName: &filePath,
	})
	if err != nil {
		return nil, err
	}

	// Iterate through the file history to find the last commit
	var lastCommit *object.Commit
	err = history.ForEach(func(c *object.Commit) error {
		lastCommit = c
		return nil
	})

	// Check for the "EOF" error and ignore it
	if err != nil {
		// if err == io.EOF {
		// 	err = nil
		// } else {
		return nil, err
		// }
	}

	return lastCommit, nil
}

func (r *GitRepo) Checkout(branch string) error {
	// Get the Worktree
	wt, err := r.Repository.Worktree()
	if err != nil {
		return err
	}

	// Checkout to the specified branch
	err = wt.Checkout(&git.CheckoutOptions{
		Branch: plumbing.ReferenceName(fmt.Sprintf("refs/heads/%s", branch)),
		// Branch: plumbing.NewRemoteReferenceName("origin", branch),
		Create: false,
		Force:  true,
	})
	if err != nil {
		return err
	}

	return nil
}
