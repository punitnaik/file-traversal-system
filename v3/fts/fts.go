package fts

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/punitnaik/file-traversal-system-v3/db/arangodb"
	"gitlab.com/punitnaik/file-traversal-system-v3/db/redis"
	"gitlab.com/punitnaik/file-traversal-system-v3/utils"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
)

type FileTraverser struct {
	DB          *arangodb.ArangoDB
	Repo        *GitRepo
	RedisClient *redis.RedisClient
}

type RepoInfo struct {
	CommitID         string    `json:"commit_id"`
	LastSynced       string    `json:"last_synced"`
	RepoName         string    `json:"repo_name"`
	RepoID           string    `json:"repo_id"`
	Branches         []string  `json:"branch_names"`
	DateAdded        string    `json:"date_added"`
	BranchID         string    `json:"branch_id"`
	Branch           string    `json:"branch"`
	RepoURL          string    `json:"repo_url"`
	LivedocsBranches []string  `json:"livedocs_array"`
	CloneInfo        CloneInfo `json:"clone_info"`
}

type CloneInfo struct {
	Method   string `json:"method"`
	KeyName  string `json:"key_name"`
	Platform string `json:"platform"`
}

type FileInfo struct {
	Name       string `json:"name"`
	FilePath   string `json:"file_path"`
	ParentPath string `json:"parent_path"`
	Extension  string `json:"extension"`
	FileURL    string `json:"file_url"`
}

type RepoURL struct {
	Owner    string `json:"owner"`
	RepoName string `json:"repo_name"`
	Branch   string `json:"branch"`
	BaseURL  string `json:"base_url"`
	Platform string `json:"platform"`
	Format   string `json:"format"`
}

func NewFileTraverser(conf *utils.Config, db *arangodb.ArangoDB, repo *GitRepo) *FileTraverser {
	if db == nil {
		db = arangodb.NewArangoDB()
	}
	if repo == nil {
		repo = NewGitRepo()
	}

	redis := redis.NewRedisClient(conf)

	return &FileTraverser{
		DB:          db,
		Repo:        repo,
		RedisClient: redis,
	}

}

func (fts *FileTraverser) UpdateDatabaseInfo(Ctx context.Context, RepoInfo RepoInfo) (map[string]interface{}, error) {
	// Get Commit details
	var err error
	RepoInfo.CommitID, err = fts.Repo.GetCommitID()
	if err != nil {
		return nil, err
	}

	RepoInfo.Branches, err = fts.Repo.Branches()
	if err != nil {
		return nil, err
	}

	RepoInfo.Branch, err = fts.Repo.Branch()
	if err != nil {
		return nil, err
	}

	RepoInfo.LastSynced = utils.GetUTCTimestamp()
	if RepoInfo.DateAdded == "" {
		RepoInfo.DateAdded = RepoInfo.LastSynced
	}

	if RepoInfo.LivedocsBranches == nil || len(RepoInfo.LivedocsBranches) == 0 {
		RepoInfo.LivedocsBranches = []string{RepoInfo.Branch}
	}

	// Update the details in arangodb
	Info, err := fts.DB.LoadCollection(Ctx, arangodb.InfoCollectionName)
	if err != nil {
		return nil, err
	}

	_, err = Info.CreateDocument(Ctx, RepoInfo)
	if err != nil {
		return nil, err
	}

	return_data := map[string]interface{}{
		"external_repo_url":     RepoInfo.RepoURL,
		"date_added":            RepoInfo.DateAdded,
		"last_synced":           RepoInfo.LastSynced,
		"last_synced_commit_id": RepoInfo.CommitID,
		"current_branch":        RepoInfo.Branch,
		"repo_name":             RepoInfo.RepoName,
		"repo_id":               RepoInfo.RepoID,
		"branch":                RepoInfo.Branches,
		"commit_id":             RepoInfo.CommitID,
	}
	return return_data, nil
}

func (fts *FileTraverser) Sync(conf *utils.Config, ctx context.Context, RepoInfo *RepoInfo, user_id string) error {
	path := fmt.Sprintf("%v/%v/", conf.GetString("asvatthi.workdir"), user_id)
	repo_path := fmt.Sprintf("%v/%v/%v", path, RepoInfo.RepoID, RepoInfo.RepoName)

	// Open the repo
	if err := fts.Repo.OpenRepo(repo_path); err != nil {
		return err
	}

	var err error
	var auth transport.AuthMethod
	switch RepoInfo.CloneInfo.Method {
	case "ssh":
		key_path := fmt.Sprintf("%v%v/%v", path, conf.GetString("asvatthi.SSH-Keys"), RepoInfo.CloneInfo.KeyName)
		auth, err = SSHBasicAuth(key_path)
		if err != nil {
			return err
		}

	case "oauth":
		key := fmt.Sprintf("%v-%v", user_id, RepoInfo.CloneInfo.Platform)
		access_token, err := fts.RedisClient.GetAccessToken(context.Background(), key)
		if err != nil {
			return err
		}

		auth = OauthBasicAuth(access_token)

	default:
		return errors.New("invalid method")

	}

	// Fetch
	if err := fts.Repo.Pull(auth); err != nil {
		if err == NoErrAlreadyUpToDate {
			return nil
		}
		return err
	}

	// Get branch_id for all the livedocs branches
	var branch_ids map[string]string
	if err := fts.DB.GetBranchIDFromBranch(ctx, RepoInfo.RepoID, &branch_ids); err != nil {
		return err
	}

	// Iterate over all the livedocs branches
	for _, branch := range RepoInfo.LivedocsBranches {
		// Fetch the Changes
		changes, err := fts.GetFetchChanges(RepoInfo.CommitID, branch)
		if err != nil {
			return err
		}

		branch_id := branch_ids[branch]
		collection, err := fts.DB.LoadCollection(ctx, branch_id+arangodb.FilesSuffix)
		if err != nil {
			return err
		}

		// Extract data fron repo url
		url_info := ExtractURLInfo(RepoInfo.RepoURL)
		url_info.Branch = branch

		for _, change := range changes {
			action, err := change.Action()
			if err != nil {
				return err
			}
			switch action.String() {
			case "Insert":
				if err = fts.HandleInsertAction(ctx, change.To.Name, url_info, collection); err != nil {
					return err
				}
			case "Delete":
				if err = fts.HandleDeleteAction(ctx, change.From.Name, branch_id, url_info, collection); err != nil {
					return err
				}
			case "Modify":
				continue
			default:
				return err
			}
		}
	}

	return nil
}

// Function to read the content of a file
func ReadFileContent(filepath string) (string, error) {
	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		return "", err
	}

	data, err := os.ReadFile(filepath)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func (fts *FileTraverser) Traverse(ctx context.Context, branch_id, repo_url, branch string) error {
	Files, err := fts.DB.SetupCollections(ctx, branch_id)
	if err != nil {
		return err
	}

	// Get the HEAD reference
	ref, err := fts.Repo.Head()
	if err != nil {
		return err
	}

	// Get the commit object for the HEAD reference
	commit, err := fts.Repo.CommitObject(ref.Hash())
	if err != nil {
		return err
	}

	// Get the tree object for the commit
	tree, err := commit.Tree()
	if err != nil {
		return err
	}

	// Extract data fron repo url
	url_info := ExtractURLInfo(repo_url)
	url_info.Branch = branch

	// Traverse the tree and print the file paths
	var FileInfoSlice []FileInfo
	err = tree.Files().ForEach(func(f *object.File) error {
		file_info := GetFileInfo(f.Name, url_info)
		FileInfoSlice = append(FileInfoSlice, file_info)
		return nil
	})

	if err != nil {
		return err
	}

	_, _, err = Files.CreateDocuments(ctx, FileInfoSlice)
	if err != nil {
		return err
	}

	return nil
}

func GetFileInfo(path string, url_info RepoURL) FileInfo {
	file_name := utils.GetFileName(path)
	folder_path := utils.GetFolderPath(path)
	ext := filepath.Ext(file_name)
	file_url := CreateFileBlobURL(path, url_info)

	return FileInfo{
		FilePath:   path,
		ParentPath: folder_path,
		Extension:  ext,
		Name:       file_name,
		FileURL:    file_url,
	}
}

func CreateFileBlobURL(file_path string, repo_url RepoURL) string {
	// Fill the format str
	if repo_url.Platform == "unknown" {
		return ""
	}
	file_url := fmt.Sprintf(repo_url.Format, repo_url.BaseURL, repo_url.Owner, repo_url.RepoName, repo_url.Branch, file_path)

	// URL encode the Blob URL
	encodedURL, err := url.Parse(file_url)
	if err != nil {
		// Error occurred while parsing URL
		return ""
	}
	return encodedURL.String()
}

func ExtractURLInfo(url string) RepoURL {
	var info RepoURL

	// Regex patterns for different URL formats
	sshPattern := regexp.MustCompile(`git@([^:]+):(?P<owner>[^/]+)/(?P<repo_name>[^\.]+)\.git`)
	httpsPattern := regexp.MustCompile(`(https?://[^/]+)/(?P<owner>[^/]+)/(?P<repo_name>[^/]+)\.git`)

	// Extracting owner, name, and base URL
	if matches := sshPattern.FindStringSubmatch(url); len(matches) == 3 {
		info.Owner = matches[1]
		info.RepoName = matches[2]
		info.BaseURL = "https://" + matches[1]
	} else if matches := httpsPattern.FindStringSubmatch(url); len(matches) == 4 {
		info.BaseURL = matches[1]
		info.Owner = matches[2]
		info.RepoName = matches[3]
	}

	// Extracting platform
	switch {
	case strings.Contains(info.BaseURL, "github"):
		info.Platform = "github"
		info.Format = "%s/%s/%s/blob/%s/%s"

	case strings.Contains(info.BaseURL, "bitbucket"):
		info.Platform = "bitbucket"
		info.Format = "%s/%s/%s/src/%s/%s"

	case strings.Contains(info.BaseURL, "gitlab"):
		info.Platform = "gitlab"
		info.Format = "%s/%s/%s/-/blob/%s/%s"

	default:
		info.Platform = "unknown"
	}

	return info
}

func (fts *FileTraverser) Clone(repo_url, user_id, method string, conf *utils.Config) (RepoInfo, error) {
	repo_name := utils.ExtractRepoName(repo_url)
	repo_id := utils.GetNewUUID(utils.RepoIDPrefix)
	path := fmt.Sprintf("%v/%v", conf.GetString("asvatthi.workdir"), user_id)
	clonepath := fmt.Sprintf("%v/%v/%v", path, repo_id, repo_name)

	var err error
	var method_data CloneInfo
	var repo GitRepo
	not_cloned := true

	switch method {
	case "ssh":
		key_path := fmt.Sprintf("%v%v", path, conf.GetString("asvatthi.SSH-Keys"))

		// Get list of files in the SSH keys folder
		keyFiles, err := os.ReadDir(key_path)
		if err != nil {
			return RepoInfo{}, err
		}
		// Iterate over each key file
		for _, keyFile := range keyFiles {
			// Construct full path to the key file
			keyPath := filepath.Join(key_path, keyFile.Name())

			key, err := GetAuthDetails(keyPath)
			if err != nil {
				return RepoInfo{}, err
			}

			if err = repo.Clone(clonepath, repo_url, key); err == nil {
				method_data.KeyName = keyFile.Name()
				not_cloned = !not_cloned
				break
			}
		}

		method_data.Method = method

	case "oauth":
		platform := utils.GetHostingSite(repo_url)
		if platform == "unknown" {
			return RepoInfo{}, errors.New("invalid oauth platform")
		}

		key := fmt.Sprintf("%v-%v", user_id, platform)
		access_token, err := fts.RedisClient.GetAccessToken(context.Background(), key)
		if err != nil {
			return RepoInfo{}, err
		}

		auth := OauthBasicAuth(access_token)
		if err = repo.Clone(clonepath, repo_url, auth); err != nil {
			return RepoInfo{}, err
		}

		method_data.Method = method
		method_data.Platform = platform
		not_cloned = !not_cloned

	default:
		return RepoInfo{}, errors.New("invalid method")

	}

	if not_cloned {
		return RepoInfo{}, errors.New("unable to clone the repo")
	}

	branch, err := repo.Branch()
	if err != nil {
		return RepoInfo{}, err
	}

	fts.Repo = &repo
	return RepoInfo{
		RepoName:  repo_name,
		RepoURL:   repo_url,
		RepoID:    repo_id,
		CloneInfo: method_data,
		Branch:    branch,
	}, nil
}
