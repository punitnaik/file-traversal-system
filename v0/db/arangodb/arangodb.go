package arangodb

import (
	"context"
	"fmt"
	"log"

	driver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
	"gitlab.com/punitnaik/file-traversal-system-v0/utils"
)

type ArangoDBClient struct {
	driver.Client
}

type ArangoDB struct {
	Files    driver.Collection
	Folders  driver.Collection
	Contains driver.Collection
	driver.Database
}

type DocumentMeta struct {
	driver.DocumentMeta
}

func (a *ArangoDBClient) Init(config utils.Config) error {
	// Get configaration data
	subConfig := config.Sub("database.arangodb")
	host := subConfig.GetString("host")
	port := subConfig.GetString("port")
	username := subConfig.GetString("login")
	password := subConfig.GetString("pass")
	url := fmt.Sprintf("http://%v:%v", host, port)

	conn, err := a.CreateArangoDBConnection(url)
	if err != nil {
		return err
	}

	if err = a.CreateArangoDBClient(conn, username, password); err != nil {
		return err
	}

	return nil
}

func (a *ArangoDBClient) CreateArangoDBConnection(url string) (driver.Connection, error) {
	// Define the ArangoDB connection parameters
	conn, err := http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{url},
	})
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func (a *ArangoDBClient) CreateArangoDBClient(conn driver.Connection, username, password string) error {
	// Create an ArangoDB client
	var err error
	if a.Client, err = driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication(username, password),
	}); err != nil {
		return err
	}

	return nil
}

func (a *ArangoDBClient) CreateArangoDBDatabase(ctx context.Context, dbName string) (ArangoDB, error) {
	// Create the database
	var err error
	var db ArangoDB
	db.Database, err = a.Client.CreateDatabase(ctx, dbName, nil)
	if err != nil {
		return ArangoDB{}, err
	}

	return db, nil
}

func (a *ArangoDBClient) LoadArangoDBDatabase(ctx context.Context, dbName string) (ArangoDB, error) {
	// Load the database
	var err error
	var db ArangoDB
	db.Database, err = a.Client.Database(ctx, dbName)
	if err != nil {
		return ArangoDB{}, err
	}

	return db, nil
}

func (a *ArangoDBClient) SetupArangoDBDatabase(ctx context.Context, dbName string) (ArangoDB, error) {
	// Load the database
	db, err := a.CreateArangoDBDatabase(ctx, dbName)
	if err != nil {
		return ArangoDB{}, err
	}

	_, err = db.CreateArangoDBCollection(ctx, "Info", nil)
	if err != nil {
		return ArangoDB{}, err
	}
	return db, nil
}

func (db *ArangoDB) CreateArangoDBGraph(ctx context.Context, graphName string) (driver.Graph, error) {
	edgeDefinition := driver.EdgeDefinition{
		Collection: "Contains",
		From:       []string{"Folders"},
		To:         []string{"Folders", "Files"},
	}
	// Create the graph
	graph, err := db.Database.CreateGraph(ctx, graphName, &driver.CreateGraphOptions{
		EdgeDefinitions: []driver.EdgeDefinition{edgeDefinition},
	})
	if err != nil {
		return nil, err
	}
	return graph, nil
}

func (db *ArangoDB) CreateArangoDBCollection(ctx context.Context, collectionName string, options *driver.CreateCollectionOptions) (driver.Collection, error) {
	//create the collection
	collection, err := db.Database.CreateCollection(ctx, collectionName, options)
	if err != nil {
		return nil, err
	}

	return collection, nil
}

func (db *ArangoDB) CreateArangoDBEdgeCollection(ctx context.Context, collectionName string) (driver.Collection, error) {
	//create the collection
	options := &driver.CreateCollectionOptions{
		Type: driver.CollectionTypeEdge,
	}

	collection, err := db.CreateArangoDBCollection(ctx, collectionName, options)
	if err != nil {
		return nil, err
	}

	return collection, nil
}

func (db *ArangoDB) DeleteArangoDBCollection(ctx context.Context, collectionName string) error {
	col, err := db.Database.Collection(ctx, collectionName)
	if err != nil {
		return err
	}

	// Drop the collection
	if err := col.Remove(ctx); err != nil {
		return err
	}

	return nil
}

func CreateArangoDBDocument(ctx context.Context, collection driver.Collection, data map[string]interface{}) (driver.DocumentMeta, error) {
	//create the Document
	document, err := collection.CreateDocument(ctx, data)
	if err != nil {
		return driver.DocumentMeta{}, err
	}
	return document, nil
}

func DeleteArangoDBDocument(ctx context.Context, collection driver.Collection, key string) (driver.DocumentMeta, error) {
	// Remove the document
	document, err := collection.RemoveDocument(ctx, key)
	if err != nil {
		return driver.DocumentMeta{}, err
	}
	return document, nil
}

func CreateArangoDBEdge(ctx context.Context, collection driver.Collection, fromKey string, toKey string) (driver.DocumentMeta, error) {
	// Create the edge between the documents
	edge := map[string]interface{}{
		"_from": fromKey,
		"_to":   toKey,
	}
	edgeDoc, err := collection.CreateDocument(ctx, edge)
	if err != nil {
		return driver.DocumentMeta{}, err
	}

	return edgeDoc, nil
}

func (db *ArangoDB) QueryArangoDBDatabase(ctx context.Context, query string, bindVars map[string]interface{}) ([]map[string]interface{}, error) {
	// query := `
	// 	FOR doc IN @@collection
	// 	FILTER LIKE(doc.Name, "%.py")
	// 	RETURN doc
	// `
	// bindVars := map[string]interface{}{
	// 	"@collection": "Files",
	// }

	// Execute the query
	cursor, err := db.Database.Query(ctx, query, bindVars)
	if err != nil {
		return nil, err
	}
	defer cursor.Close()

	// Iterate over the results
	var documents []map[string]interface{}
	for {
		var doc map[string]interface{}
		_, err := cursor.ReadDocument(ctx, &doc)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		documents = append(documents, doc)
	}

	return documents, nil
}

func (db *ArangoDB) GetEmptyDocuentMeta() driver.DocumentMeta {
	return driver.DocumentMeta{}
}

func (db *ArangoDB) SetupCollections(ctx context.Context, repo_id string) error {
	var err error
	db.Files, err = db.CreateArangoDBCollection(ctx, repo_id+"Files", nil)
	if err != nil {
		return err
	}

	db.Folders, err = db.CreateArangoDBCollection(ctx, repo_id+"Folders", nil)
	if err != nil {
		return err
	}

	db.Contains, err = db.CreateArangoDBEdgeCollection(ctx, repo_id+"Contains")
	if err != nil {
		return err
	}

	_, err = db.CreateArangoDBGraph(ctx, repo_id+"Tree")
	if err != nil {
		return err
	}

	return nil
}

func (db *ArangoDB) LoadCollections(ctx context.Context) error {
	var err error
	db.Files, err = db.Database.Collection(ctx, "Files")
	if err != nil {
		return err
	}

	db.Folders, err = db.Database.Collection(ctx, "Folders")
	if err != nil {
		return err
	}

	db.Contains, err = db.Database.Collection(ctx, "Contains")
	if err != nil {
		return err
	}

	return nil
}

func (a *ArangoDBClient) GetDatabase(ctx context.Context, name string) (ArangoDB, error) {
	var err error
	var db ArangoDB
	db.Database, err = a.Database(ctx, name)
	if err != nil {
		return ArangoDB{}, err
	}

	return db, nil
}

// func test() {
// 	query := driver.NewQuery("FOR doc IN @@collection SORT doc._modified DESC LIMIT 1 RETURN doc").BindVars(map[string]interface{}{
// 		"@collection": "Info",
// 	})
// }

func (db *ArangoDB) GetRepoName(ctx context.Context, repoId string) (string, error) {
	// Query the database to get last modified entry to get the information about the last updated commit
	cursor, err := db.Database.Query(ctx, GetRepoNameFromRepoID, map[string]interface{}{
		"@collection": "Info",
		"repoID":      repoId,
	})
	if err != nil {
		return "", err
	}
	defer cursor.Close()

	// Read the result
	var repoName string
	_, err = cursor.ReadDocument(ctx, &repoName)
	if err != nil {
		return "", err
	}

	return repoName, nil
}

func (db *ArangoDB) GetLastCommitID(ctx context.Context, repoId string) (string, error) {
	// Query the database to get last modified entry to get the information about the last updated commit
	cursor, err := db.Database.Query(ctx, GetLastCommitFromRepoID, map[string]interface{}{
		"@collection": "Info",
		"repoID":      repoId,
	})
	if err != nil {
		return "", err
	}
	defer cursor.Close()

	// Read the result
	var commitID string
	_, err = cursor.ReadDocument(ctx, &commitID)
	if err != nil {
		return "", err
	}

	return commitID, nil
}
