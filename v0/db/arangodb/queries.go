package arangodb

const GetDocByExt = `
	FOR doc IN @@collection
	FILTER doc.extension == @extension
	RETURN doc
`

const Query = `
	FOR doc IN @@collection
	COLLECT fileType = doc.extension WITH COUNT INTO count
	RETURN { fileType, count }
`
const GetDocByFilepath = `
	FOR doc IN @@collection
	FILTER doc.Path == @fileName
	RETURN doc
`

const GetLastDocument = `
	FOR doc IN @@collection 
	SORT doc._modified DESC 
	LIMIT 1 
	RETURN doc
`
const GetDocFromPath = `
	FOR doc IN @@collection 
	FILTER doc.Path == @filename 
	LIMIT 1
	RETURN doc
`
const GetContainsDocFrom_to = `
	FOR doc IN @@collection 
	FILTER doc._to == @ID 
	LIMIT 1
	RETURN doc
`

const GetRepoNameFromRepoID = `
	FOR doc IN @@collection
	FILTER doc.RepoID == @repoID
	LIMIT 1
	RETURN doc.RepoName
`
const GetLastCommitFromRepoID = `
	FOR doc IN @@collection
	FILTER doc.RepoID == @repoID
	SORT doc._modified DESC
	LIMIT 1
	RETURN doc.CommitHash
`
