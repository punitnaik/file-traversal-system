package main

import (
	"gitlab.com/punitnaik/file-traversal-system-v0/db/arangodb"
	"gitlab.com/punitnaik/file-traversal-system-v0/server"
	"gitlab.com/punitnaik/file-traversal-system-v0/utils"
)

func main() {
	// Config
	Config := &utils.Config{}
	err := Config.Init()
	if err != nil {
		panic(err)
	}

	// ArangoDB
	ArangoDB := &arangodb.ArangoDBClient{}
	err = ArangoDB.Init(*Config)
	if err != nil {
		panic(err)
	}

	s := &server.Server{
		ArangoDB: *ArangoDB,
		Config:   *Config,
	}
	s.Start()
}
