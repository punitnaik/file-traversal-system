package middleware

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

type Payload struct {
	UserID  string `json:"user_id" binding:"required"`
	RepoURL string `json:"repo_url"`
	FileID  string `json:"file_id"`
	RepoID  string `json:"repo_id"`
	Branch  string `json:"branch"`
}

type Auth struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	UserID   string `json:"user_id" binding:"required"`
	RepoURL  string `json:"repo_url"`
	FileID   string `json:"file_id"`
	RepoID   string `json:"repo_id"`
	Branch   string `json:"branch"`
}

// Authorize is a middleware function for session management using JWT
func Authorize() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get the JSON from the request body
		var requestBody interface{}
		if err := c.ShouldBindJSON(&requestBody); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "unable to get request data"})
			return
		}
		payload := requestBody.(map[string]interface{})

		// Get the username and password from environment variables
		username := os.Getenv("FTS_USER")
		password := os.Getenv("FTS_PASSWD")

		// Check if both environment variables are set
		if username == "" || password == "" {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Username or Password is not set"})
			return
		} else if username != payload["username"].(string) || password != payload["password"].(string) {
			// Compare it with the request data
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Username or Password does not match"})
			return
		}
		delete(payload, "username")
		delete(payload, "password")

		for _, value := range payload {
			if value.(string) == "" {
				c.JSON(http.StatusInternalServerError, gin.H{"error": "Incomplete data"})
				return
			}
		}

		// Set user ID in the Gin context
		c.Set("Payload", payload)
		c.Next()
	}
}
