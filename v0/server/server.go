package server

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/punitnaik/file-traversal-system-v0/db/arangodb"
	"gitlab.com/punitnaik/file-traversal-system-v0/server/middleware"
	"gitlab.com/punitnaik/file-traversal-system-v0/utils"
)

// Server serves requests to DB with router
type Server struct {
	ArangoDB arangodb.ArangoDBClient
	// Logger      log.Logger
	Config utils.Config
}

func (s *Server) defineRoutes(router *gin.Engine) {
	repo := router.Group("/repo")
	repo.Use(middleware.Authorize())
	repo.DELETE("", s.DeleteRepo())
	repo.POST("/info", s.SSHFetch())
	repo.POST("/checkout", s.SSHFetch())
	repo.POST("/files", s.SSHFetch())

	ssh := repo.Group("/ssh")
	ssh.POST("/clone", s.SSHClone())
	ssh.POST("/fetch", s.SSHFetch())

	gitlab := repo.Group("/ssh")
	gitlab.POST("/clone", s.SSHClone())
	gitlab.POST("/fetch", s.SSHFetch())

	file := router.Group("/file")
	file.POST("/content", s.SSHFetch())
}

func (s *Server) setupRouter() *gin.Engine {
	r := gin.Default()

	r.Use(gin.Recovery())

	s.defineRoutes(r)
	return r
}

func (s *Server) Start() {
	r := s.setupRouter()
	PORT := ":7349"
	srv := &http.Server{
		Addr:    PORT,
		Handler: r,
	}

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf(err.Error())
		}
	}()

	log.Printf("Server started on %s\n", PORT)

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")
}

func GetRequestPayload(c *gin.Context) (map[string]interface{}, error) {
	var requestBody interface{}
	if err := c.ShouldBindJSON(&requestBody); err != nil {
		return nil, err
	}

	return requestBody.(map[string]interface{}), nil
}
