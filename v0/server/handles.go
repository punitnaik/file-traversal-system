package server

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/punitnaik/file-traversal-system-v0/db/arangodb"
	"gitlab.com/punitnaik/file-traversal-system-v0/fts"
	"gitlab.com/punitnaik/file-traversal-system-v0/utils"
	gitutils "gitlab.com/punitnaik/file-traversal-system-v0/utils/git"
)

// func (s *Server) Sync() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		// Get payload from request body
// 		requestBody, ok := c.Get("Payload")
// 		if !ok {
// 			c.JSON(http.StatusInternalServerError, gin.H{"error": "The payload is inaccurate"})
// 			return
// 		}
// 		payload := requestBody.(middleware.Payload)

// 		// Get arangoDB database from client
// 		arangodb, err := s.ArangoDB.GetDatabase(c.Request.Context(), payload.Name)
// 		if err != nil {
// 			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error(), "msg": "Unable to get arangodb"})
// 			return
// 		}

// 		// Get last commit id from the database
// 		lastCommit, err := fts.GetLastCommitID(c.Request.Context(), &arangodb)
// 		if err != nil {
// 			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error(), "msg": "Unable to get the last commit id"})
// 			return
// 		}

// 		f := fts.FileTraverser{
// 			RepoPath: payload.Path,
// 			DB:       &arangodb,
// 			Ctx:      c.Request.Context(),
// 		}

// 		// Fetch the Changes
// 		changes, err := fts.GetFetchChanges(payload.Path, lastCommit)
// 		if err != nil {
// 			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error(), "msg": "Unable to get the changes"})
// 			return
// 		}

// 		for _, change := range changes {
// 			action, err := change.Action()
// 			if err != nil {
// 				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error(), "msg": "Unable to get action"})
// 				return
// 			}
// 			switch action.String() {
// 			case "Insert":
// 				if err = f.HandleInsertAction(change.To.Name); err != nil {
// 					c.JSON(http.StatusBadRequest, gin.H{"error": err.Error(), "msg": "Insert action failed"})
// 					return
// 				}
// 			case "Delete":
// 				if err = f.HandleDeleteAction(change.From.Name); err != nil {
// 					c.JSON(http.StatusBadRequest, gin.H{"error": err.Error(), "msg": "Delete action failed"})
// 					return
// 				}
// 			case "Modify":
// 				continue
// 			default:
// 				c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error(), "msg": "Invalid action"})
// 				return
// 			}
// 		}

// 		// Update the arangodb about latest changes
// 		if err = fts.UpdateDatabaseInfo(c.Request.Context(), &arangodb, payload.Path); err != nil {
// 			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error(), "msg": "Unable to update arangodb info"})
// 			return
// 		}

// 		c.JSON(http.StatusOK, gin.H{"status": "successfully synchronized the data with ArangoDB"})
// 	}
// }

func (s *Server) SSHClone() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get payload from request body
		requestBody, ok := c.Get("Payload")
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		user_id, ok := payload["user_id"]
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		repo_url, ok := payload["repo_url"]
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		key_name, ok := payload["key_name"]
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}

		path := s.Config.GetString("asvatthi.workdir") + user_id.(string) + "/"
		repoName := utils.ExtractRepoName(repo_url.(string))
		clonepath := path + repoName
		var repo gitutils.GitRepo

		cloneOptions, err := repo.SSHCloneOptions(repo_url.(string), path+"SpecialFolder/"+key_name.(string)+".pem")
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "unable to create clone options"})
			return
		}

		err = repo.Clone(clonepath, cloneOptions)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "unable to clone the repo"})
			return
		}

		// Check if the user already has a database
		exists, err := s.ArangoDB.Client.DatabaseExists(c.Request.Context(), user_id.(string))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "unable to check if the database exists or not"})
			return
		}

		// Set up and Load the data to arangoDB
		var arangodb arangodb.ArangoDB
		if exists {
			arangodb, err = s.ArangoDB.SetupArangoDBDatabase(c.Request.Context(), user_id.(string))
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"error": "unable to load arangodb database"})
				return
			}
		} else {
			arangodb, err = s.ArangoDB.CreateArangoDBDatabase(c.Request.Context(), user_id.(string))
			if err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{"error": "unable to create arangodb database"})
				return
			}
		}

		// Create a id for repo
		repoId := utils.GenerateRandomString(16, "alphanumeric")
		err = arangodb.SetupCollections(c.Request.Context(), repoId)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "unable to create arangodb collections"})
			return
		}

		f := fts.FileTraverser{
			RepoPath: clonepath,
			DB:       &arangodb,
			Ctx:      c.Request.Context(),
		}

		branches, commitID, err := f.Traverse(repoId, repoName)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "unable to traverse the repo"})
			return
		}

		c.JSON(http.StatusOK, gin.H{"local_repo_id": repoId, "repo_name": repoName, "branch": branches, "last_synced_commit_id": commitID, "status": "Successfully loaded the data to Arangodb"})
	}
}

func (s *Server) DeleteRepo() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get payload from request body
		requestBody, ok := c.Get("Payload")
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		repoId, ok := payload["repo_id"]
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		user_id, ok := payload["user_id"]
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}

		// Get arangoDB database from client
		arangodb, err := s.ArangoDB.GetDatabase(c.Request.Context(), user_id.(string))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error(), "status": "Unable to get arangodb"})
			return
		}

		path := s.Config.GetString("asvatthi.workdir") + user_id.(string) + "/"
		repoName, err := arangodb.GetRepoName(c.Request.Context(), repoId.(string))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error(), "status": "Unable to get data from arangodb"})
			return
		}

		if err := os.Remove(path + repoName); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "Repo deletion failed due to Server error"})
			return
		}

		c.JSON(http.StatusOK, gin.H{"status": "Successfully deleted repo"})
	}
}

func (s *Server) SSHFetch() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get payload from request body
		requestBody, ok := c.Get("Payload")
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		payload := requestBody.(map[string]interface{})

		user_id, ok := payload["user_id"]
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		repoId, ok := payload["repo_id"]
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}
		key_name, ok := payload["key_name"]
		if !ok {
			c.JSON(http.StatusBadRequest, gin.H{"error": "The payload is inaccurate"})
			return
		}

		// Get arangoDB database from client
		arangodb, err := s.ArangoDB.GetDatabase(c.Request.Context(), user_id.(string))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error(), "status": "Unable to get arangodb"})
			return
		}

		path := s.Config.GetString("asvatthi.workdir") + user_id.(string) + "/"
		repoName, err := arangodb.GetRepoName(c.Request.Context(), repoId.(string))
		repoPath := path + repoName
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error(), "status": "Unable to get data from arangodb"})
			return
		}

		// Open the repo
		var repo gitutils.GitRepo
		if err := repo.OpenRepo(repoPath); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error(), "status": "unable to open the repo"})
			return
		}

		// Create fetch options
		fetchOptions, err := repo.SSHFetchOptions(path + "SpecialFolder/" + key_name.(string) + ".pem")
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error(), "status": "unable to create fetch options"})
			return
		}

		// Fetch
		if err := repo.Pull(fetchOptions); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error(), "status": "unable to fetch the changes"})
			return
		}

		f := fts.FileTraverser{
			RepoPath: repoPath,
			DB:       &arangodb,
			Ctx:      c.Request.Context(),
			Repo:     repo,
		}

		branches, commitID, err := f.Sync(repoId.(string), repoName)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "unable to sync the repo"})
			return
		}

		c.JSON(http.StatusOK, gin.H{"branch": branches, "last_synced_commit_id": commitID, "status": "Successfully updated the data."})
	}
}
