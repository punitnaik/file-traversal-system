package gitutils

import (
	"golang.org/x/crypto/ssh"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/config"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
	gitssh "gopkg.in/src-d/go-git.v4/plumbing/transport/ssh"
)

type GitRepo struct {
	*git.Repository
}

func Checkout(worktree *git.Worktree, targetBranch string) error {
	// Checkout the target branch
	err := worktree.Checkout(&git.CheckoutOptions{
		Branch: plumbing.ReferenceName("refs/heads/" + targetBranch),
		Create: true,
		Force:  false,
	})
	if err != nil {
		return err
	}

	return nil
}

func GetWorktree(repo *git.Repository) (*git.Worktree, error) {
	// Get the worktree
	worktree, err := repo.Worktree()
	if err != nil {
		return nil, err
	}

	return worktree, nil
}

func FillCloneOptions(values map[string]interface{}) *git.CloneOptions {
	// Create a map with values to fill the CloneOptions struct
	// auth := &http.BasicAuth{
	// 		Username: "oauth2",
	// 		Password: token.AccessToken,
	// }
	// optionsMap := map[string]interface{}{
	// 	"url":  "https://github.com/example/repo.git",
	// 	"auth": auth,
	// }
	options := &git.CloneOptions{}

	if values == nil {
		return options
	}

	if url, ok := values["url"].(string); ok {
		options.URL = url
	}

	if auth, ok := values["auth"].(http.AuthMethod); ok {
		options.Auth = auth
	}

	// if remoteName, ok := values["remoteName"].(string); ok {
	// 	options.RemoteName = remoteName
	// }

	// if referenceName, ok := values["referenceName"].(plumbing.ReferenceName); ok {
	// 	options.ReferenceName = referenceName
	// }

	// if singleBranch, ok := values["singleBranch"].(bool); ok {
	// 	options.SingleBranch = singleBranch
	// }

	// if noCheckout, ok := values["noCheckout"].(bool); ok {
	// 	options.NoCheckout = noCheckout
	// }

	// if depth, ok := values["depth"].(int); ok {
	// 	options.Depth = depth
	// }

	// if recurseSubmodules, ok := values["recurseSubmodules"].(SubmoduleRescursivity); ok {
	// 	options.RecurseSubmodules = recurseSubmodules
	// }

	// if progress, ok := values["progress"].(sideband.Progress); ok {
	// 	options.Progress = progress
	// }

	// if tags, ok := values["tags"].(TagMode); ok {
	// 	options.Tags = tags.(git.TagMode)
	// }

	return options
}

func FillFetchOptions(values map[string]interface{}) *git.FetchOptions {
	options := &git.FetchOptions{}

	if values == nil {
		return options
	}

	if remoteName, ok := values["remoteName"].(string); ok {
		options.RemoteName = remoteName
	}

	if refSpecs, ok := values["refSpecs"].([]config.RefSpec); ok {
		options.RefSpecs = refSpecs
	} else {
		options.RefSpecs = []config.RefSpec{"refs/*:refs/*"}
	}

	if auth, ok := values["auth"].(transport.AuthMethod); ok {
		options.Auth = auth
	}

	// if tags, ok := values["tags"].(TagMode); ok {
	// 	options.Tags = tags
	// }

	// if depth, ok := values["depth"].(int); ok {
	// 	options.Depth = depth
	// }

	// if shallow, ok := values["shallow"].(bool); ok {
	// 	options.Shallow = shallow
	// }

	// if singleBranch, ok := values["singleBranch"].(bool); ok {
	// 	options.SingleBranch = singleBranch
	// }

	// if noTags, ok := values["noTags"].(bool); ok {
	// 	options.NoTags = noTags
	// }

	// if prune, ok := values["prune"].(bool); ok {
	// 	options.Prune = prune
	// }

	// if progress, ok := values["progress"].(sideband.Progress); ok {
	// 	options.Progress = progress
	// }

	return options
}

func (r *GitRepo) Clone(path string, cloneOptions *git.CloneOptions) error {
	// Clone the repository
	var err error
	if r.Repository, err = git.PlainClone(path, false, cloneOptions); err != nil {
		return err
	}
	return nil
}

func (r *GitRepo) Pull(fetchOptions *git.FetchOptions) error {
	// Fetch the latest changes from the remote repository
	if err := r.Repository.Fetch(fetchOptions); err != git.NoErrAlreadyUpToDate || err != nil {
		return err
	}

	return nil
}

func (r *GitRepo) OpenRepo(repoPath string) error {
	// Open the repository
	var err error
	r.Repository, err = git.PlainOpen(repoPath)
	if err != nil {
		return err
	}

	return nil
}

func GetCommitID(repo *git.Repository) (string, error) {
	headref, err := repo.Head()
	if err != nil {
		return "", err
	}

	return headref.Hash().String(), nil
}

func (r *GitRepo) GetCommitHash() (plumbing.Hash, error) {
	headref, err := r.Repository.Head()
	if err != nil {
		return plumbing.Hash{}, err
	}

	return headref.Hash(), nil
}

func GetCommitTime(repo *git.Repository) (string, error) {
	// Retrieve the commit object at the HEAD reference.
	headRef, err := repo.Head()
	if err != nil {
		return "", err
	}

	commitObj, err := repo.CommitObject(headRef.Hash())
	if err != nil {
		return "", err
	}

	return commitObj.Committer.When.UTC().Format("2006-01-02T15:04:05.999999999Z07:00"), nil
}

func GetBranchName(repo *git.Repository) (string, error) {
	headRef, err := repo.Head()
	if err != nil {
		return "", err
	}

	return headRef.Name().Short(), nil
}

func Branch(repo *git.Repository) ([]string, error) {
	// Retrieve all references (branches, tags, etc.)
	refs, err := repo.References()
	if err != nil {
		return nil, err
	}

	var branches []string

	// Iterate through references to find branches
	err = refs.ForEach(func(ref *plumbing.Reference) error {
		if ref.Name().IsBranch() {
			branches = append(branches, ref.Name().Short())
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return branches, nil
}

func GetHash(commitID string) plumbing.Hash {
	return plumbing.NewHash(commitID)
}

func (r *GitRepo) GetCommitObject() (*object.Commit, error) {
	commitHash, err := r.GetCommitHash()
	if err != nil {
		return nil, err
	}
	commit, err := r.Repository.CommitObject(commitHash)
	if err != nil {
		return nil, err
	}

	return commit, nil
}

func (r *GitRepo) GetCommitObjectFromID(commitHash plumbing.Hash) (*object.Commit, error) {
	commit, err := r.Repository.CommitObject(commitHash)
	if err != nil {
		return nil, err
	}

	return commit, nil
}

func (r *GitRepo) OauthCloneOptions(repoUrl, access_token string) *git.CloneOptions {
	return &git.CloneOptions{
		URL: repoUrl,
		Auth: &http.BasicAuth{
			Username: "oauth2",
			Password: access_token,
		},
	}
}

func (r *GitRepo) OauthFetchOptions(access_token string) *git.FetchOptions {
	return &git.FetchOptions{
		Auth: &http.BasicAuth{
			Username: "oauth2",
			Password: access_token,
		},
		RefSpecs: []config.RefSpec{"refs/*:refs/*"},
	}
}

func (r *GitRepo) SSHCloneOptions(repoUrl, keyPath string) (*git.CloneOptions, error) {
	auth, err := GetAuthDetails(keyPath)
	if err != nil {
		return nil, err
	}
	return &git.CloneOptions{
		URL:  repoUrl,
		Auth: auth,
	}, nil
}

func (r *GitRepo) SSHFetchOptions(keyPath string) (*git.FetchOptions, error) {
	auth, err := GetAuthDetails(keyPath)
	if err != nil {
		return nil, err
	}
	return &git.FetchOptions{
		RemoteName: "origin",
		Auth:       auth,
	}, nil
}

func GetAuthDetails(keyPath string) (*gitssh.PublicKeys, error) {
	// Set up SSH authentication
	auth, err := gitssh.NewPublicKeysFromFile("git", keyPath, "")
	if err != nil {
		return nil, err
	}
	auth.HostKeyCallback = ssh.InsecureIgnoreHostKey()

	return auth, nil
}

func (repo *GitRepo) GetGitRelatedInfo(filePath string) (*object.Commit, error) {
	// Get the file history
	history, err := repo.Repository.Log(&git.LogOptions{
		FileName: &filePath,
	})
	if err != nil {
		return nil, err
	}

	// Iterate through the file history to find the last commit
	var lastCommit *object.Commit
	err = history.ForEach(func(c *object.Commit) error {
		lastCommit = c
		return nil
	})

	// Check for the "EOF" error and ignore it
	if err != nil {
		// if err == io.EOF {
		// 	err = nil
		// } else {
		return nil, err
		// }
	}

	return lastCommit, nil
}
