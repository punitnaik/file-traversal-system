package fts

import (
	"context"
	"errors"
	"path/filepath"

	"gitlab.com/punitnaik/file-traversal-system-v0/db/arangodb"
	"gitlab.com/punitnaik/file-traversal-system-v0/utils"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

func GetDiffTree(start, end *object.Commit) (object.Changes, error) {
	// Get the tree of each commit
	tree1, err := end.Tree()
	if err != nil {
		return nil, err
	}

	tree2, err := start.Tree()
	if err != nil {
		return nil, err
	}

	// Get the changes between the two trees
	changes, err := object.DiffTree(tree1, tree2)
	if err != nil {
		return nil, err
	}

	return changes, nil
}

func (fts *FileTraverser) GetFetchChanges(commitID string) (object.Changes, error) {
	lastCommit, err := fts.Repo.GetCommitObjectFromID(plumbing.NewHash(commitID))
	if err != nil {
		return nil, err
	}

	latestCommit, err := fts.Repo.GetCommitObject()
	if err != nil {
		return nil, err
	}

	changes, err := GetDiffTree(latestCommit, lastCommit)
	if err != nil {
		return nil, err
	}

	return changes, nil
}

func (fts *FileTraverser) HandleInsertAction(path, repo_id string) error {
	// Get the folder document
	folderPath := utils.GetFolderPath(path)
	cursor, err := fts.DB.Database.Query(fts.Ctx, arangodb.GetDocFromPath, map[string]interface{}{
		"@collection": repo_id + "Folders",
		"filename":    folderPath,
	})
	if err != nil {
		return err
	}

	var folder arangodb.DocumentMeta
	if !cursor.HasMore() {
		// Create a new document
		return errors.New("TODO")
	} else {
		// Read the result
		_, err = cursor.ReadDocument(fts.Ctx, &folder.DocumentMeta)
		if err != nil {
			return err
		}
	}
	defer cursor.Close()

	// Get the data related to file
	file, err := fts.GetFileInfo(path)
	if err != nil {
		return err
	}
	Filedata := map[string]interface{}{
		"Name":      file.Name(),
		"Path":      path,
		"Extension": filepath.Ext(file.Name()),
	}
	// Create a new document in Files Collection
	_, err = AddEntry(fts.Ctx, Filedata, folder.DocumentMeta, fts.DB.Files, fts.DB.Contains)
	if err != nil {
		return err
	}

	return nil
}

func (fts *FileTraverser) HandleDeleteAction(path, repo_id string) error {
	// Get the file document
	cursor, err := fts.DB.Database.Query(fts.Ctx, arangodb.GetDocFromPath, map[string]interface{}{
		"@collection": repo_id + "Files",
		"filename":    path,
	})
	if err != nil {
		return err
	}

	// Read the result
	var file arangodb.DocumentMeta
	_, err = cursor.ReadDocument(fts.Ctx, &file.DocumentMeta)
	if err != nil {
		return err
	}
	cursor.Close()

	// Get the contains document
	cursor, err = fts.DB.Database.Query(fts.Ctx, arangodb.GetContainsDocFrom_to, map[string]interface{}{
		"@collection": repo_id + "Contains",
		"ID":          file.ID.String(),
	})
	if err != nil {
		return err
	}

	// Read the result
	var contains arangodb.DocumentMeta
	_, err = cursor.ReadDocument(fts.Ctx, &contains.DocumentMeta)
	if err != nil {
		return err
	}

	// Delete both Documents
	_, err = fts.DB.Files.RemoveDocument(fts.Ctx, file.Key)
	if err != nil {
		return err
	}
	_, err = fts.DB.Contains.RemoveDocument(fts.Ctx, contains.Key)
	if err != nil {
		return err
	}

	defer cursor.Close()
	return nil
}

func HandleModifyAction(ctx context.Context, db *arangodb.ArangoDB, path string) error {
	return nil
}
