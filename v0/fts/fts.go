package fts

import (
	"context"
	"io/fs"
	"os"
	"path/filepath"
	"time"

	"github.com/arangodb/go-driver"
	"gitlab.com/punitnaik/file-traversal-system-v0/db/arangodb"
	gitutils "gitlab.com/punitnaik/file-traversal-system-v0/utils/git"
	"gopkg.in/src-d/go-git.v4/plumbing"
)

type FileTraverser struct {
	RepoPath string
	DB       *arangodb.ArangoDB
	Ctx      context.Context
	Repo     gitutils.GitRepo
}

func (fts *FileTraverser) GetFileInfo(path string) (fs.FileInfo, error) {
	file, err := os.Stat(fts.RepoPath + "/" + path)
	if err != nil {
		return nil, err
	}

	return file, nil
}

func (fts *FileTraverser) Open(path string) (*os.File, error) {
	dir, err := os.Open(fts.RepoPath + "/" + path)
	if err != nil {
		return nil, err
	}

	return dir, nil
}

func (fts *FileTraverser) Traverse(repo_id, repo_name string) ([]string, string, error) {
	if err := fts.Repo.OpenRepo(fts.RepoPath); err != nil {
		return nil, "", err
	}

	if err := fts.RecursiveTraverse("", driver.DocumentMeta{}); err != nil {
		return nil, "", err
	}

	branches, commitId, err := fts.UpdateDatabaseInfo(repo_id, repo_name)
	if err != nil {
		return nil, "", err
	}

	return branches, commitId, nil
}

func (fts *FileTraverser) RecursiveTraverse(path string, parent driver.DocumentMeta) error {
	file, err := fts.GetFileInfo(path)
	if err != nil {
		return err
	}

	Filedata := map[string]interface{}{
		"Name": file.Name(),
		"Path": path,
	}

	if !file.IsDir() {
		extension := filepath.Ext(file.Name())
		// gitInfo, err := fts.Repo.GetGitRelatedInfo(path)
		// if err != nil {
		// 	Filedata["CommitHash"] = nil
		// 	Filedata["Modified"] = nil
		// } else {
		// 	Filedata["CommitHash"] = gitInfo.Hash
		// 	Filedata["Modified"] = gitInfo.Committer.When
		// }
		Filedata["Extension"] = extension

		if _, err = AddEntry(fts.Ctx, Filedata, parent, fts.DB.Files, fts.DB.Contains); err != nil {
			return err
		}

		return nil
	}

	FoldDoc, err := AddEntry(fts.Ctx, Filedata, parent, fts.DB.Folders, fts.DB.Contains)
	if err != nil {
		return err
	}

	// Open the directory
	dir, err := fts.Open(path)
	if err != nil {
		return err
	}
	defer dir.Close()

	// Read the directory contents
	files, err := dir.Readdir(-1)
	if err != nil {
		return err
	}
	for _, file := range files {
		// filePath := path + "/" + file.Name()
		var filePath string
		if file.Name() == ".git" {
			continue
		}
		if path != "" {
			filePath = path + "/" + file.Name()
		} else {
			filePath = file.Name()
		}
		if err = fts.RecursiveTraverse(filePath, FoldDoc); err != nil {
			return err
		}
	}

	return nil
}

func AddEntry(ctx context.Context, data map[string]interface{}, parent driver.DocumentMeta, dir, Contains driver.Collection) (driver.DocumentMeta, error) {
	FileDoc, err := arangodb.CreateArangoDBDocument(ctx, dir, data)
	if err != nil {
		return driver.DocumentMeta{}, err
	}

	if !parent.ID.IsEmpty() {
		_, err = arangodb.CreateArangoDBEdge(ctx, Contains, string(parent.ID), string(FileDoc.ID))
		if err != nil {
			return driver.DocumentMeta{}, err
		}
	}

	return FileDoc, err
}

func (fts *FileTraverser) UpdateDatabaseInfo(repo_id, repo_name string) ([]string, string, error) {
	// Get Commit details
	commitID, err := fts.Repo.GetCommitHash()
	if err != nil {
		return nil, "", err
	}

	branches, err := fts.Repo.Branches()
	if err != nil {
		return nil, "", err
	}

	// Iterate over the branches and print their names
	var branchNames []string
	err = branches.ForEach(func(ref *plumbing.Reference) error {
		// Reference name is in the format "refs/heads/branch_name"
		branchName := ref.Name().Short()
		branchNames = append(branchNames, branchName)
		return nil
	})
	if err != nil {
		return nil, "", err
	}

	RepoInfo := map[string]interface{}{
		"CommitHash": commitID.String(),
		"_modified":  time.Now(),
		"RepoName":   repo_name,
		"RepoID":     repo_id,
		"Branches":   branchNames,
	}

	// Update the details in arangodb
	Info, err := fts.DB.Database.Collection(fts.Ctx, "Info")
	if err != nil {
		return nil, "", err
	}

	_, err = Info.CreateDocument(fts.Ctx, RepoInfo)
	if err != nil {
		return nil, "", err
	}

	return branchNames, commitID.String(), nil
}

func (fts *FileTraverser) Sync(repo_id, repo_name string) ([]string, string, error) {
	// Get last commit id from the database
	lastCommit, err := fts.DB.GetLastCommitID(fts.Ctx, repo_id)
	if err != nil {
		return nil, "", err
	}

	// Fetch the Changes
	changes, err := fts.GetFetchChanges(lastCommit)
	if err != nil {
		return nil, "", err
	}

	for _, change := range changes {
		action, err := change.Action()
		if err != nil {
			return nil, "", err
		}
		switch action.String() {
		case "Insert":
			if err = fts.HandleInsertAction(change.To.Name, repo_id); err != nil {
				return nil, "", err
			}
		case "Delete":
			if err = fts.HandleDeleteAction(change.From.Name, repo_id); err != nil {
				return nil, "", err
			}
		case "Modify":
			continue
		default:
			return nil, "", err
		}
	}

	branches, commitId, err := fts.UpdateDatabaseInfo(repo_id, repo_name)
	if err != nil {
		return nil, "", err
	}

	return branches, commitId, nil
}
